import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/widgets/index.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Components/index.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';

class ScholarshipNavigatorForDesktop extends StatelessWidget {
  ScholarshipNavigatorForDesktop({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
    @required this.scholarshipState,
    @required this.scholarshipModel,
  });

  ScholarshipBloc scholarshipBloc;
  ScholarshipState scholarshipState;
  ScholarshipModel scholarshipModel;

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  @override
  Widget build(BuildContext context) {
    print("_____ScholarshipNavigatorForDesktop________");
    return Material(
      color: Colors.transparent,
      child: Container(
        width: double.maxFinite,
        color: scholarshipPageColors.panelBackgroundColor,
        child: Material(
          color: Colors.transparent,
          child: Column(
            children: <Widget>[
              SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
              Container(
                decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(width: scholarshipPageStyles.shareWidthDp, color: scholarshipPageColors.secondaryColor))),
                child: Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          scholarshipState.selectDetailTab[scholarshipModel.id] = 1;
                          scholarshipBloc.add(SelectDetailTabEvent(id: scholarshipModel.id, tapIndex: 1));
                        },
                        child: Container(
                          padding: EdgeInsets.only(bottom: 5),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: scholarshipPageStyles.lineHeight,
                                color: (scholarshipState.selectDetailTab[scholarshipModel.id] == 1)
                                    ? scholarshipPageColors.secondaryColor
                                    : scholarshipPageColors.panelBackgroundColor,
                              ),
                            ),
                          ),
                          child: Text(
                            "ScholarshipNavigatorForDesktop.menu1".tr(),
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.titleFontSize,
                              color: scholarshipPageColors.textColor,
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          scholarshipState.selectDetailTab[scholarshipModel.id] = 2;
                          scholarshipBloc.add(SelectDetailTabEvent(id: scholarshipModel.id, tapIndex: 2));
                        },
                        child: Container(
                          padding: EdgeInsets.only(bottom: 5),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: 3,
                                color: (scholarshipState.selectDetailTab[scholarshipModel.id] == 2)
                                    ? scholarshipPageColors.secondaryColor
                                    : scholarshipPageColors.panelBackgroundColor,
                              ),
                            ),
                          ),
                          child: Text(
                            "ScholarshipNavigatorForDesktop.menu2".tr(),
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.titleFontSize,
                              color: scholarshipPageColors.textColor,
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          scholarshipState.selectDetailTab[scholarshipModel.id] = 3;
                          scholarshipBloc.add(SelectDetailTabEvent(id: scholarshipModel.id, tapIndex: 3));
                        },
                        child: Container(
                          padding: EdgeInsets.only(bottom: 5),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: 3,
                                color: (scholarshipState.selectDetailTab[scholarshipModel.id] == 3)
                                    ? scholarshipPageColors.secondaryColor
                                    : scholarshipPageColors.panelBackgroundColor,
                              ),
                            ),
                          ),
                          child: Text(
                            "ScholarshipNavigatorForDesktop.menu3".tr(),
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.titleFontSize,
                              color: scholarshipPageColors.textColor,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              (scholarshipState.selectDetailTab[scholarshipModel.id] == 1)
                  ? AboutUsPanel(
                      scholarshipModel: scholarshipModel,
                      scholarshipPageColors: scholarshipPageColors,
                      scholarshipBloc: scholarshipBloc,
                      scholarshipPageStyles: scholarshipPageStyles,
                      scholarshipState: scholarshipState,
                    )
                  : (scholarshipState.selectDetailTab[scholarshipModel.id] == 2)
                      ? EligibilityPanelForDesktop(
                          scholarshipPageStyles: scholarshipPageStyles,
                          scholarshipPageColors: scholarshipPageColors,
                          scholarshipBloc: scholarshipBloc,
                          scholarshipState: scholarshipState,
                          scholarshipModel: scholarshipModel,
                        )
                      : SubmitApplicationPanel(
                          scholarshipPageStyles: scholarshipPageStyles,
                          scholarshipPageColors: scholarshipPageColors,
                          scholarshipBloc: scholarshipBloc,
                          scholarshipState: scholarshipState,
                          scholarshipModel: scholarshipModel,
                        ),
            ],
          ),
        ),
      ),
    );
  }
}
