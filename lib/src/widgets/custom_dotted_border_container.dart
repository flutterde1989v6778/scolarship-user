import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:provider/provider.dart';

class CustomDottedBorderContainer extends StatelessWidget {
  CustomDottedBorderContainer({
    Key key,
    @required this.width,
    @required this.height,
    @required this.child,
    this.horizontal = 20,
    this.vertical = 20,
    this.backgroundColor = Colors.white,
    this.title = "",
    this.titleMargin = 10,
    this.titlePadding = 10,
    this.titleTextFontSize = 15,
    this.titleTextColor = Colors.black,
    this.dottedBorderColor = Colors.black,
    this.borderWidth = 1,
    this.padding = const EdgeInsets.all(10),
    this.dashPattern = const [10, 10],
  }) : super(key: key);
  final double width;
  final double height;
  final double horizontal;
  final double vertical;
  final Widget child;
  final Color backgroundColor;
  final String title;
  final double titleMargin;
  final double titlePadding;
  final double titleTextFontSize;
  final Color titleTextColor;
  final Color dottedBorderColor;
  final double borderWidth;
  final EdgeInsetsGeometry padding;
  final List<double> dashPattern;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: Material(
        color: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Material(
              color: Colors.transparent,
              child: Column(
                children: <Widget>[
                  SizedBox(height: titleTextFontSize / 2),
                  DottedBorder(
                    borderType: BorderType.Rect,
                    dashPattern: dashPattern,
                    strokeCap: StrokeCap.square,
                    color: dottedBorderColor,
                    strokeWidth: borderWidth,
                    child: Container(
                      width: width,
                      height: (title != "") ? height - titleTextFontSize / 2 - borderWidth * 4 : height,
                      padding: padding,
                      color: backgroundColor,
                      child: Center(child: child),
                    ),
                  ),
                ],
              ),
            ),
            (title != "")
                ? Container(
                    margin: EdgeInsets.symmetric(horizontal: titleMargin),
                    padding: EdgeInsets.symmetric(horizontal: titlePadding),
                    color: backgroundColor,
                    child: Text(title, style: TextStyle(fontSize: titleTextFontSize, color: titleTextColor)),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}

class CustomDottedBorderContainerProvider extends ChangeNotifier {
  static CustomDottedBorderContainerProvider of(BuildContext context, {bool listen = false}) =>
      Provider.of<CustomDottedBorderContainerProvider>(context, listen: listen);
}
