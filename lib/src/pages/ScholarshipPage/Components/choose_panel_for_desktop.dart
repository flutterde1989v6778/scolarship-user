import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';

class ChoosePanelForDesktop extends StatelessWidget {
  ChoosePanelForDesktop({
    Key key,
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.selectScholarshipAnimationController,
    @required this.selectScholarshipColorTween,
  }) : super(key: key);

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  AnimationController selectScholarshipAnimationController;
  Animation selectScholarshipColorTween;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        width: scholarshipPageStyles.deviceWidth,
        padding: EdgeInsets.symmetric(horizontal: scholarshipPageStyles.choosePanelHorizontalMargin),
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(
            horizontal: scholarshipPageStyles.choosePanelHorizontalPadding,
            vertical: scholarshipPageStyles.choosePanelVerticalPadding,
          ),
          decoration: BoxDecoration(
            color: scholarshipPageColors.panelBackgroundColor,
            border: Border(top: BorderSide(width: scholarshipPageStyles.lineHeight, color: scholarshipPageColors.secondaryColor)),
          ),
          alignment: Alignment.topCenter,
          child: Material(
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      selectScholarshipAnimationController.forward();
                    },
                    child: Material(
                      color: Colors.transparent,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Material(
                            color: Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "ChoosePanel.findScholarshipTitle".tr(),
                                  style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.secondaryColor),
                                ),
                                Icon(
                                  Icons.search,
                                  size: scholarshipPageStyles.titleFontSize * 2,
                                  color: scholarshipPageColors.secondaryColor,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                          Text(
                            "ChoosePanel.findScholarshipDescription".tr(),
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.textFontSize,
                              color: scholarshipPageColors.textColor,
                              height: scholarshipPageStyles.choosePanelLineSpacing,
                              letterSpacing: scholarshipPageStyles.choosePanelLetterSpacing,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: scholarshipPageStyles.choosePanelHorizontalPadding * 1.5),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      selectScholarshipAnimationController.forward();
                    },
                    child: Material(
                      color: Colors.transparent,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Material(
                            color: Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "ChoosePanel.submitApplicationTitle".tr(),
                                  style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.secondaryColor),
                                ),
                                Icon(
                                  Icons.move_to_inbox,
                                  size: scholarshipPageStyles.titleFontSize * 2,
                                  color: scholarshipPageColors.secondaryColor,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                          Text(
                            "ChoosePanel.submitApplicationDescription".tr(),
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.textFontSize,
                              color: scholarshipPageColors.textColor,
                              height: scholarshipPageStyles.choosePanelLineSpacing,
                              letterSpacing: scholarshipPageStyles.choosePanelLetterSpacing,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: scholarshipPageStyles.choosePanelHorizontalPadding * 1.5),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      selectScholarshipAnimationController.forward();
                    },
                    child: Material(
                      color: Colors.transparent,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Material(
                            color: Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "ChoosePanel.getResponseTitle".tr(),
                                  style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.secondaryColor),
                                ),
                                Icon(
                                  Icons.notifications_active,
                                  size: scholarshipPageStyles.titleFontSize * 2,
                                  color: scholarshipPageColors.secondaryColor,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                          Text(
                            "ChoosePanel.getResponseDescription".tr(),
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.textFontSize,
                              color: scholarshipPageColors.textColor,
                              height: scholarshipPageStyles.choosePanelLineSpacing,
                              letterSpacing: scholarshipPageStyles.choosePanelLetterSpacing,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
