import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/utils/date_time_convert.dart';
import 'package:scholarship/src/widgets/index.dart';
import "package:universal_html/html.dart" as html;
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Components/index.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';
import 'package:scholarship/src/widgets/youtube.dart';

class ScholarshipForDesktop extends StatelessWidget {
  ScholarshipForDesktop({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
    @required this.scholarshipState,
    @required this.scholarshipModel,
    @required this.selectScholarshipAnimationController,
    @required this.selectScholarshipColorTween,
  });

  ScholarshipBloc scholarshipBloc;
  ScholarshipState scholarshipState;
  ScholarshipModel scholarshipModel;

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  AnimationController selectScholarshipAnimationController;
  Animation selectScholarshipColorTween;

  @override
  Widget build(BuildContext context) {
    print("______ScholarshipForDesktop________");
    return Material(
      color: Colors.transparent,
      child: Container(
        width: scholarshipPageStyles.scholarshipWidth,
        margin: EdgeInsets.symmetric(
          vertical: scholarshipPageStyles.scholarshipVerticalMargin,
        ),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.symmetric(
              horizontal: BorderSide(width: scholarshipPageStyles.shareWidthDp, color: scholarshipPageColors.scholarshipShadowColor.withAlpha(128))),
          boxShadow: [
            BoxShadow(
              color: scholarshipPageColors.scholarshipShadowColor,
              offset: new Offset(0, scholarshipPageStyles.shareWidthDp * 2),
              blurRadius: scholarshipPageStyles.shareWidthDp * 2,
              spreadRadius: 0,
            ),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: (scholarshipPageStyles.scholarshipWidth) *
                    ((scholarshipState.isOpenedDetailBody[scholarshipModel.id] != null && scholarshipState.isOpenedDetailBody[scholarshipModel.id])
                        ? 1
                        : 0.3),
                height: scholarshipPageStyles.lineHeight,
                color: scholarshipPageColors.secondaryColor,
              ),
              SizedBox(height: scholarshipPageStyles.shareWidthDp * 12),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: scholarshipPageStyles.scholarshipHorizontalPadding,
                  vertical: scholarshipPageStyles.scholarshipVerticalPadding,
                ),
                decoration: BoxDecoration(
                  color: scholarshipPageColors.panelBackgroundColor,
                ),
                child: Material(
                  color: Colors.transparent,
                  child: Row(
                    children: <Widget>[
                      CustomNetworkImage(
                        url: scholarshipModel.imageUrl,
                        width: scholarshipPageStyles.scholarshipImageWidth,
                        height: scholarshipPageStyles.scholarshipImageHeight,
                      ),
                      SizedBox(width: scholarshipPageStyles.scholarshipHorizontalPadding),
                      Expanded(
                        child: Container(
                          height: scholarshipPageStyles.scholarshipImageHeight,
                          child: Material(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  scholarshipModel.title,
                                  style: TextStyle(
                                    fontSize: scholarshipPageStyles.titleFontSize,
                                    color: scholarshipPageColors.textColor,
                                  ),
                                ),
                                Text(
                                  scholarshipModel.description,
                                  style: TextStyle(
                                    fontSize: scholarshipPageStyles.textFontSize,
                                    color: scholarshipPageColors.textColor,
                                    letterSpacing: 1.5,
                                    height: 1.5,
                                  ),
                                ),
                                Material(
                                  color: Colors.transparent,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Material(
                                        color: Colors.transparent,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              "Scholarship.status".tr(
                                                args: [(scholarshipModel.isOpened) ? "Scholarship.openStatus".tr() : "Scholarship.closeStatus".tr()],
                                              ),
                                              style: TextStyle(
                                                fontSize: scholarshipPageStyles.textFontSize,
                                                color: scholarshipPageColors.textColor,
                                              ),
                                            ),
                                            SizedBox(width: 15),
                                            Text(
                                              "Scholarship.deadLines".tr(
                                                args: [
                                                  "${convertMillisecondsToDateTime(scholarshipModel.deadlines).difference(DateTime.now()).inDays}",
                                                ],
                                              ),
                                              style: TextStyle(
                                                fontSize: scholarshipPageStyles.textFontSize,
                                                color: scholarshipPageColors.textColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      AnimatedBuilder(
                                        animation: selectScholarshipColorTween,
                                        builder: (BuildContext context, Widget child) {
                                          return GestureDetector(
                                            onTap: () {
                                              if ((scholarshipState.isOpenedDetailBody[scholarshipModel.id] != null &&
                                                  scholarshipState.isOpenedDetailBody[scholarshipModel.id])) {
                                                scholarshipBloc.add(DetailBodyIsOpenedEvent(id: scholarshipModel.id, isOpened: false));
                                              } else {
                                                scholarshipBloc.add(DetailBodyIsOpenedEvent(id: scholarshipModel.id, isOpened: true));
                                              }
                                            },
                                            child: Container(
                                              color: selectScholarshipColorTween.value,
                                              padding: EdgeInsets.all(3),
                                              child: Material(
                                                color: Colors.transparent,
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(
                                                      "Scholarship.applyButtonTextForDesktop".tr(),
                                                      style: TextStyle(
                                                        fontSize: scholarshipPageStyles.scholarshipDetailLinkTextFontSize,
                                                        color: scholarshipPageColors.secondaryColor,
                                                      ),
                                                    ),
                                                    SizedBox(width: 5),
                                                    Icon(
                                                      ((scholarshipState.isOpenedDetailBody[scholarshipModel.id] != null &&
                                                              scholarshipState.isOpenedDetailBody[scholarshipModel.id])
                                                          ? Icons.remove
                                                          : Icons.add),
                                                      size: scholarshipPageStyles.scholarshipDetailLinkTextFontSize,
                                                      color: scholarshipPageColors.secondaryColor,
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              (scholarshipState.isOpenedDetailBody[scholarshipModel.id] != null && scholarshipState.isOpenedDetailBody[scholarshipModel.id])
                  ? ScholarshipNavigatorForDesktop(
                      scholarshipPageStyles: scholarshipPageStyles,
                      scholarshipPageColors: scholarshipPageColors,
                      scholarshipBloc: scholarshipBloc,
                      scholarshipState: scholarshipState,
                      scholarshipModel: scholarshipModel,
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
