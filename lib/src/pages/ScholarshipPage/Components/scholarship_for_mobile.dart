import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/widgets/custom_raised_button.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Components/index.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';
import 'package:scholarship/src/widgets/index.dart';

class ScholarshipForMobile extends StatelessWidget {
  ScholarshipForMobile({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
    @required this.scholarshipState,
    @required this.scholarshipModel,
    @required this.selectScholarshipAnimationController,
    @required this.selectScholarshipColorTween,
  });

  ScholarshipBloc scholarshipBloc;
  ScholarshipState scholarshipState;
  ScholarshipModel scholarshipModel;

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  AnimationController selectScholarshipAnimationController;
  Animation selectScholarshipColorTween;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        width: scholarshipPageStyles.deviceWidth,
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: scholarshipPageStyles.scholarshipHorizontalMargin,
            vertical: scholarshipPageStyles.scholarshipVerticalMargin,
          ),
          child: Material(
            color: Colors.transparent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CustomNetworkImage(
                  url: scholarshipModel.imageUrl,
                  width: scholarshipPageStyles.scholarshipImageWidth,
                  height: scholarshipPageStyles.scholarshipImageHeight,
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 25),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          scholarshipModel.title,
                          style: TextStyle(
                            fontSize: scholarshipPageStyles.scholarshipTitleFontSize,
                            color: scholarshipPageColors.textColor,
                          ),
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 15),
                      AnimatedBuilder(
                        animation: selectScholarshipColorTween,
                        builder: (BuildContext context, Widget child) {
                          return CustomRaisedButton(
                            width: scholarshipPageStyles.shareWidthDp * 92,
                            height: scholarshipPageStyles.shareWidthDp * 28,
                            color: selectScholarshipColorTween.value,
                            borderRadius: 3,
                            borderColor: selectScholarshipColorTween.value,
                            child: Text(
                              "Scholarship.applyButtonTextForMobile".tr(),
                              style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: Colors.white),
                            ),
                            onPressed: () {
                              if ((scholarshipState.isOpenedDetailBody[scholarshipModel.id] != null &&
                                  scholarshipState.isOpenedDetailBody[scholarshipModel.id])) {
                                scholarshipBloc.add(DetailBodyIsOpenedEvent(id: scholarshipModel.id, isOpened: false));
                              } else {
                                scholarshipBloc.add(DetailBodyIsOpenedEvent(id: scholarshipModel.id, isOpened: true));
                              }
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
                Divider(height: 1, thickness: 1, color: scholarshipPageColors.secondaryColor),
                (scholarshipState.isOpenedDetailBody[scholarshipModel.id] != null && scholarshipState.isOpenedDetailBody[scholarshipModel.id])
                    ? ScholarshipNavigatorForMobile(
                        scholarshipPageStyles: scholarshipPageStyles,
                        scholarshipPageColors: scholarshipPageColors,
                        scholarshipBloc: scholarshipBloc,
                        scholarshipState: scholarshipState,
                        scholarshipModel: scholarshipModel,
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
