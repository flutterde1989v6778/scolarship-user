import 'dart:math';

import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import "package:universal_html/html.dart" as html;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scholarship/src/widgets/index.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';

class AboutUsPanel extends StatelessWidget {
  AboutUsPanel({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
    @required this.scholarshipState,
    @required this.scholarshipModel,
  });
  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;
  ScholarshipBloc scholarshipBloc;
  ScholarshipState scholarshipState;
  ScholarshipModel scholarshipModel;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        width: scholarshipPageStyles.scholarshipWidth,
        padding: EdgeInsets.symmetric(
          horizontal: scholarshipPageStyles.scholarshipHorizontalPadding,
          vertical: scholarshipPageStyles.scholarshipVerticalPadding,
        ),
        color: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
            ? scholarshipPageColors.backgroundColor
            : scholarshipPageColors.panelBackgroundColor,
        child: Material(
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Container(
                  width: scholarshipPageStyles.shcolarshipVideoWidth,
                  height: scholarshipPageStyles.shcolarshipVideoWidth * 9 / 16,
                  alignment: Alignment.center,
                  child: Youtube(
                    element: Random.secure().nextInt(23412434).toInt().toString(),
                    url: "https://www.youtube.com/embed/ZtfItHwFlZ8",
                    size: scholarshipPageStyles.shcolarshipVideoWidth,
                  ),
                ),
              ),
              SizedBox(height: scholarshipPageStyles.scholarshipVerticalPadding * 1.5),
              Text(
                scholarshipModel.subTitle1ForAbout,
                style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.textColor),
              ),
              SizedBox(height: scholarshipPageStyles.scholarshipVerticalPadding),
              Text(
                scholarshipModel.subDescription1ForAbout,
                style: TextStyle(
                  fontSize: scholarshipPageStyles.textFontSize,
                  color: scholarshipPageColors.textColor,
                  letterSpacing: 1.5,
                  height: 1.5,
                ),
              ),
              SizedBox(height: scholarshipPageStyles.scholarshipVerticalPadding * 1.5),
              Text(
                scholarshipModel.subTitle2ForAbout,
                style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.textColor),
              ),
              SizedBox(height: scholarshipPageStyles.scholarshipVerticalPadding),
              Text(
                scholarshipModel.subDescription2ForAbout,
                style: TextStyle(
                  fontSize: scholarshipPageStyles.textFontSize,
                  color: scholarshipPageColors.textColor,
                  letterSpacing: 1.5,
                  height: 1.5,
                ),
              ),
              SizedBox(height: scholarshipPageStyles.scholarshipVerticalPadding),
            ],
          ),
        ),
      ),
    );
  }
}
