import 'dart:async';

import 'package:scholarship/src/models/index.dart';

import 'index.dart';

class ScholarshipApplicationProvider extends FirebaseProvider {
  final String collectionName = "/ScholarshipApplications";
  FirebaseProvider firebaseProvider = FirebaseProvider();

  Future<ScholarshipApplicationModel> addScholarshipApplication({ScholarshipApplicationModel scholarshipApplicationModel}) async {
    try {
      var result = await firebaseProvider.addDocument(collectionName: collectionName, data: scholarshipApplicationModel.toJson());
      if (result != "-1")
        return ScholarshipApplicationModel.fromJson(result);
      else
        return null;
    } catch (e) {
      print("addScholarshipApplication error");
      print(e);
      return null;
    }
  }

  Future<bool> updateScholarshipApplication({ScholarshipApplicationModel scholarshipApplicationModel}) async {
    return await firebaseProvider.updateDocument(
        collectionName: collectionName, id: scholarshipApplicationModel.id, data: scholarshipApplicationModel.toJson());
  }

  Future<bool> deleteScholarshipApplication({ScholarshipApplicationModel scholarshipApplicationModel}) async {
    return await firebaseProvider.deleteDocument(collectionName: collectionName, id: scholarshipApplicationModel.id);
  }

  Stream<List<ScholarshipApplicationModel>> getScholarshipApplicationListStream({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) {
    try {
      Stream<List<Map<String, dynamic>>> stream =
          firebaseProvider.getDocumentsStream(collectionName: collectionName, wheres: wheres, orderby: orderby, limit: limit);
      return stream.map((dataList) {
        return dataList.map((data) {
          return ScholarshipApplicationModel.fromJson(data);
        }).toList();
      });
    } catch (e) {
      return null;
    }
  }

  Future<List<ScholarshipApplicationModel>> getScholarshipApplicationList({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) async {
    List<ScholarshipApplicationModel> scholarshipApplicationModelList = [];
    var result = await firebaseProvider.getDocumentData(collectionName: collectionName, wheres: wheres, orderby: orderby, limit: limit);
    if (result != "-1") {
      result.forEach((data) {
        scholarshipApplicationModelList.add(ScholarshipApplicationModel.fromJson(data));
      });
      return scholarshipApplicationModelList;
    } else {
      return null;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }
}
