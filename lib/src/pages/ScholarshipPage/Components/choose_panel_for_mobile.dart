import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class ChoosePanelForMobile extends StatelessWidget {
  ChoosePanelForMobile({
    Key key,
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.selectScholarshipAnimationController,
    @required this.selectScholarshipColorTween,
  }) : super(key: key);

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  AnimationController selectScholarshipAnimationController;
  Animation selectScholarshipColorTween;

  PageController _carouselController = PageController(viewportFraction: 0.8);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: scholarshipPageStyles.primaryHorizontalPadding),
            child: Text(
              "ChoosePanel.carouselTitle".tr(),
              style: TextStyle(
                fontSize: scholarshipPageStyles.bigTitleFontSize,
                color: scholarshipPageColors.textColor,
              ),
            ),
          ),
          SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
          SizedBox(
            height: scholarshipPageStyles.carouselHeight,
            child: PageView(
              controller: _carouselController,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    selectScholarshipAnimationController.forward();
                  },
                  child: Container(
                    width: scholarshipPageStyles.carouselWidth,
                    height: scholarshipPageStyles.carouselHeight,
                    margin: EdgeInsets.only(right: scholarshipPageStyles.carouselMargin),
                    padding: EdgeInsets.symmetric(
                      horizontal: scholarshipPageStyles.choosePanelHorizontalPadding,
                      vertical: scholarshipPageStyles.choosePanelVerticalPadding,
                    ),
                    decoration: BoxDecoration(
                      color: scholarshipPageColors.panelBackgroundColor,
                      border: Border(top: BorderSide(width: 3, color: scholarshipPageColors.secondaryColor)),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: Stack(
                        children: <Widget>[
                          Material(
                            color: Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Icon(
                                  Icons.search,
                                  size: scholarshipPageStyles.carouselIconSize,
                                  color: scholarshipPageColors.carouselIconColor,
                                ),
                              ],
                            ),
                          ),
                          Material(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ChoosePanel.findScholarshipTitle".tr(),
                                  style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.secondaryColor),
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Text(
                                  "ChoosePanel.findScholarshipDescription".tr(),
                                  style: TextStyle(
                                    fontSize: scholarshipPageStyles.textFontSize,
                                    color: scholarshipPageColors.textColor,
                                    letterSpacing: 1.2,
                                    height: 1.2,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    selectScholarshipAnimationController.forward();
                  },
                  child: Container(
                    width: scholarshipPageStyles.carouselWidth,
                    height: scholarshipPageStyles.carouselHeight,
                    margin: EdgeInsets.symmetric(horizontal: scholarshipPageStyles.carouselMargin),
                    padding: EdgeInsets.symmetric(
                      horizontal: scholarshipPageStyles.choosePanelHorizontalPadding,
                      vertical: scholarshipPageStyles.choosePanelVerticalPadding,
                    ),
                    decoration: BoxDecoration(
                      color: scholarshipPageColors.panelBackgroundColor,
                      border: Border(top: BorderSide(width: 3, color: scholarshipPageColors.secondaryColor)),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: Stack(
                        children: <Widget>[
                          Material(
                            color: Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Icon(
                                  Icons.move_to_inbox,
                                  size: scholarshipPageStyles.carouselIconSize,
                                  color: scholarshipPageColors.carouselIconColor,
                                ),
                              ],
                            ),
                          ),
                          Material(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ChoosePanel.submitApplicationTitle".tr(),
                                  style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.secondaryColor),
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Text(
                                  "ChoosePanel.submitApplicationDescription".tr(),
                                  style: TextStyle(
                                    fontSize: scholarshipPageStyles.textFontSize,
                                    color: scholarshipPageColors.textColor,
                                    letterSpacing: 1.2,
                                    height: 1.2,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    selectScholarshipAnimationController.forward();
                  },
                  child: Container(
                    width: scholarshipPageStyles.carouselWidth,
                    height: scholarshipPageStyles.carouselHeight,
                    margin: EdgeInsets.only(left: scholarshipPageStyles.carouselMargin),
                    padding: EdgeInsets.symmetric(
                      horizontal: scholarshipPageStyles.choosePanelHorizontalPadding,
                      vertical: scholarshipPageStyles.choosePanelVerticalPadding,
                    ),
                    decoration: BoxDecoration(
                      color: scholarshipPageColors.panelBackgroundColor,
                      border: Border(top: BorderSide(width: 3, color: scholarshipPageColors.secondaryColor)),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: Stack(
                        children: <Widget>[
                          Material(
                            color: Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Icon(
                                  Icons.notifications_active,
                                  size: scholarshipPageStyles.carouselIconSize,
                                  color: scholarshipPageColors.carouselIconColor,
                                ),
                              ],
                            ),
                          ),
                          Material(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "ChoosePanel.getResponseTitle".tr(),
                                  style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.secondaryColor),
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Text(
                                  "ChoosePanel.getResponseDescription".tr(),
                                  style: TextStyle(
                                    fontSize: scholarshipPageStyles.textFontSize,
                                    color: scholarshipPageColors.textColor,
                                    letterSpacing: 1.2,
                                    height: 1.2,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            alignment: Alignment.center,
            child: SmoothPageIndicator(
              controller: _carouselController,
              count: 3,
              effect: ExpandingDotsEffect(
                expansionFactor: 3,
                dotColor: scholarshipPageColors.indicatorColor,
                activeDotColor: scholarshipPageColors.indicatorColor,
                dotWidth: 6,
                dotHeight: 6,
                radius: 3,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
