class ScholarshipPageString {
  static String eligibilityCard1Description = "Get an exclusive multi-day clinic from the renowned Buck Brannaman.";
  static String eligibilityCard2Description = "A scholarship experience for Hunter/Jumper riders ages 13-30.";
  static String eligibilityCard3Description = "Travel to a live clinic experience to advance your horsemanship.";

  static String eligibilitySubTitle1 = "THE\nBENEFITS";
  static String eligibilitySubTitle2 = "THE\nREQUIREMENTS";
  static String eligibilitySubTitle3 = "THE\nDATES & TIMES";

  static String eligibilityDescription1 =
      "We are excited to announce our first scholarship opportunity. The winner of this scholarship will receive:\n\n• Clinic fee for one horse/rider combination to attend one of the following Buck Brannaman “Horsemanship” Clinics:\n    • Date & Location TBD (Foundation Horsemanship or Horsemanship 1)\n    • Date &  Location TBD (Horsemanship 1)\n    • Date & Location TBD (Foundation Horsemanship or Horsemanship 1)\n• One 3-day pass for an additional auditor\n• Dinner passes for 2 people to Saturday Night BBQ \n• VIP Pass to attend one dinner with Buck Brannaman \n• Stabling fee for one horse from Thursday-Sunday ";
  static String eligibilityDescription2 =
      "We recognize that there are a lot of talented, deserving riders for this scholarship. Our first recipient will follow these general guidelines: \n• Currently a Hunter/Jumper between the ages of 13 and 30\n• Must be able to attend one of the clinics described in the award\n• Must be able to provide a horse to ride in the clinic, including:\n    • All meals and transportation of horse";
  static String eligibilityDescription3 = "• Application deadline: TBD\n• Recipient notification: TBD ";
}
