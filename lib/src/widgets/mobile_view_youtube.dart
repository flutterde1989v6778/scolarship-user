import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MobileViewYoutube extends StatefulWidget {
  final String url;
  final double size;

  MobileViewYoutube({
    @required this.url,
    @required this.size
  });

  @override
  _MobileViewYoutubeState createState() => _MobileViewYoutubeState();
}

class _MobileViewYoutubeState extends State<MobileViewYoutube> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.size,
      height: widget.size * 9 / 16,
      child: WebView(
        initialUrl: Uri.dataFromString(
          '''
            <iframe src="${widget.url}" width="100%" height="100%" frameborder="0" style='background-color: #000000;' allowfullscreen></iframe>
          ''', 
          mimeType: 'text/html'
        ).toString(),
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}