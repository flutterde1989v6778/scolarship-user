import 'package:flutter/material.dart';

class AdminAlertColors {
  static Color backgroundColor1 = Color(0xFF1B7FA7);
  static Color backgroundColor2 = Color(0xFFBB4444);
}
