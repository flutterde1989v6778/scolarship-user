import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:scholarship/src/models/index.dart';
import 'package:universal_html/html.dart' as html;

@immutable
class ScholarshipState {
  final int loadingStateForScholarshipList; //// 0: init, 1: loading, -1: failed 2: success
  final Stream<List<ScholarshipModel>> scholarshipListStream;
  final Map<String, bool> isOpenedDetailBody;
  final Map<String, int> selectDetailTab;
  final Map<String, Map<int, bool>> selectEligibility;
  final Map<String, dynamic> scholarshipApplicationData;
  final int submittingStateForApplication; //// 0: init, 1: submitting, -1: fail, 2: success
  final bool submissionSuccessViewState;

  ScholarshipState({
    @required this.loadingStateForScholarshipList,
    @required this.scholarshipListStream,
    @required this.isOpenedDetailBody,
    @required this.selectDetailTab,
    @required this.selectEligibility,
    @required this.scholarshipApplicationData,
    @required this.submittingStateForApplication,
    @required this.submissionSuccessViewState,
  });

  factory ScholarshipState.init() {
    return ScholarshipState(
      loadingStateForScholarshipList: 0,
      scholarshipListStream: null,
      isOpenedDetailBody: Map<String, bool>(),
      selectDetailTab: Map<String, int>(),
      selectEligibility: Map<String, Map<int, bool>>(),
      scholarshipApplicationData: Map<String, dynamic>(),
      submittingStateForApplication: 0,
      submissionSuccessViewState: false,
    );
  }

  ScholarshipState update({
    int loadingStateForScholarshipList,
    Stream<List<ScholarshipModel>> scholarshipListStream,
    Map<String, bool> isOpenedDetailBody,
    Map<String, int> selectDetailTab,
    Map<String, Map<int, bool>> selectEligibility,
    Map<String, dynamic> scholarshipApplicationData,
    int submittingStateForApplication,
    bool submissionSuccessViewState,
  }) {
    return copyWith(
      loadingStateForScholarshipList: loadingStateForScholarshipList,
      scholarshipListStream: scholarshipListStream,
      isOpenedDetailBody: isOpenedDetailBody,
      selectDetailTab: selectDetailTab,
      selectEligibility: selectEligibility,
      scholarshipApplicationData: scholarshipApplicationData,
      submittingStateForApplication: submittingStateForApplication,
      submissionSuccessViewState: submissionSuccessViewState,
    );
  }

  ScholarshipState copyWith({
    int loadingStateForScholarshipList,
    Stream<List<ScholarshipModel>> scholarshipListStream,
    Map<String, bool> isOpenedDetailBody,
    Map<String, int> selectDetailTab,
    Map<String, Map<int, bool>> selectEligibility,
    Map<String, bool> formValidateForSubmission,
    Map<String, dynamic> scholarshipApplicationData,
    int submittingStateForApplication,
    bool submissionSuccessViewState,
  }) {
    return ScholarshipState(
      loadingStateForScholarshipList: loadingStateForScholarshipList ?? this.loadingStateForScholarshipList,
      scholarshipListStream: scholarshipListStream ?? this.scholarshipListStream,
      isOpenedDetailBody: isOpenedDetailBody ?? this.isOpenedDetailBody,
      selectDetailTab: selectDetailTab ?? this.selectDetailTab,
      selectEligibility: selectEligibility ?? this.selectEligibility,
      scholarshipApplicationData: scholarshipApplicationData ?? this.scholarshipApplicationData,
      submittingStateForApplication: submittingStateForApplication ?? this.submittingStateForApplication,
      submissionSuccessViewState: submissionSuccessViewState ?? this.submissionSuccessViewState,
    );
  }

  @override
  String toString() {
    return '''ScholarshipState {
      loadingStateForScholarshipList: $loadingStateForScholarshipList,
      isOpenedDetailBody: ${isOpenedDetailBody.toString()},
      selectDetailTab: ${selectDetailTab.toString()},
      selectEligibility: ${selectEligibility.toString()},
    }''';
  }
}
