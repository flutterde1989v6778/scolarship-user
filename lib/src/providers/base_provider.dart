import 'dart:html';
import 'dart:typed_data';

import 'package:flutter/material.dart';

abstract class BaseProvider {
  void dispose();
}

abstract class BaseFirebaseProvider extends BaseProvider {
  Future getDocumentData({
    @required String collectionName,
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  });

  Stream<List<Map<String, dynamic>>> getDocumentsStream({
    @required String collectionName,
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  });

  Future addDocument({
    @required String collectionName,
    @required Map<String, dynamic> data,
  });
  Future updateDocument({
    @required String collectionName,
    @required String id,
    @required Map<String, dynamic> data,
  });
  Future deleteDocument({
    @required String collectionName,
    @required String id,
  });
}

abstract class BaseStorageProvider extends BaseProvider {
  Future<String> uploadFileObject({@required String path, @required String fileName, @required File file});
  Future<String> uploadByteData({@required String path, @required String fileName, @required Uint8List byteData});
}
