class ScholarshipPageConstants {
  static Pattern emailPattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  static RegExp emailRegex = new RegExp(emailPattern);

  static List<Map<String, dynamic>> disciplineList = [
    {"value": "discipline 1", "text": "discipline 1"},
    {"value": "discipline 2", "text": "discipline 2"},
    {"value": "discipline 3", "text": "discipline 3"},
    {"value": "discipline 4", "text": "discipline 4"},
    {"value": "discipline 5", "text": "discipline 5"},
  ];
}
