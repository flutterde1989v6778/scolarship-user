import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scholarship/src/assets/images.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

class BannerForMobile extends StatefulWidget {
  BannerForMobile({
    Key key,
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
  }) : super(key: key);

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  @override
  _BannerForMobileState createState() => _BannerForMobileState();
}

class _BannerForMobileState extends State<BannerForMobile> with TickerProviderStateMixin {
  AnimationController _bannerTextOpacityAimationController;
  AnimationController _bannerTextPositionAimationController;
  Animation _bannerTextOpacityAnimationTween;
  Animation _bannerTextPositionAnimationTween;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _bannerTextOpacityAimationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _bannerTextOpacityAnimationTween = Tween<double>(begin: 0, end: 1).animate(_bannerTextOpacityAimationController);

    _bannerTextPositionAimationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _bannerTextPositionAnimationTween =
        Tween<double>(begin: 0, end: widget.scholarshipPageStyles.shareWidthDp * 100).animate(_bannerTextPositionAimationController);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _bannerTextOpacityAimationController.dispose();
    _bannerTextPositionAimationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            width: widget.scholarshipPageStyles.bannerImageWidth,
            height: widget.scholarshipPageStyles.bannerImageHeight,
            child: Image.asset(
              bannerPortraitImage,
              width: widget.scholarshipPageStyles.bannerImageWidth,
              height: widget.scholarshipPageStyles.bannerImageHeight,
              fit: BoxFit.fitHeight,
              filterQuality: FilterQuality.low,
            ),
          ),
          Positioned(
            child: Container(
              width: widget.scholarshipPageStyles.bannerImageWidth,
              height: widget.scholarshipPageStyles.bannerImageHeight,
              decoration: BoxDecoration(gradient: widget.scholarshipPageColors.bannerMobileGradient),
            ),
          ),
          AnimatedBuilder(
            animation: _bannerTextOpacityAimationController,
            builder: (BuildContext context, Widget child) {
              return Positioned(
                bottom: widget.scholarshipPageStyles.bannerTextBottomPadding,
                left: widget.scholarshipPageStyles.bannerTextLeftPadding,
                child: Opacity(
                  opacity: _bannerTextOpacityAnimationTween.value,
                  child: Container(
                    width: widget.scholarshipPageStyles.bannerTextWidth,
                    padding: EdgeInsets.only(top: widget.scholarshipPageStyles.shareWidthDp * 10),
                    decoration: BoxDecoration(
                      border: Border(top: BorderSide(width: widget.scholarshipPageStyles.lineHeight, color: widget.scholarshipPageColors.textColor)),
                    ),
                    child: Text(
                      "Banner.bannerDescription".tr(),
                      style: TextStyle(
                          fontSize: widget.scholarshipPageStyles.bannerDescriptionFontSize,
                          color: widget.scholarshipPageColors.textColor,
                          height: 1.5,
                          letterSpacing: 1.2),
                    ),
                  ),
                ),
              );
            },
          ),
          AnimatedBuilder(
            animation: _bannerTextPositionAimationController,
            builder: (BuildContext context, Widget child) {
              return Positioned(
                bottom: widget.scholarshipPageStyles.bannerTextBottomPadding + _bannerTextPositionAnimationTween.value,
                left: widget.scholarshipPageStyles.bannerTextLeftPadding,
                child: Container(
                  width: widget.scholarshipPageStyles.bannerTextWidth,
                  child: Material(
                    color: Colors.transparent,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Material(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Banner.bannerTitle1".tr(),
                                  style: TextStyle(
                                    fontSize: widget.scholarshipPageStyles.bannerTitleFontSize,
                                    color: widget.scholarshipPageColors.textColor,
                                  ),
                                ),
                                SizedBox(height: widget.scholarshipPageStyles.shareWidthDp * 5),
                                Text(
                                  "Banner.bannerTitle2".tr(),
                                  style: TextStyle(
                                    fontSize: widget.scholarshipPageStyles.bannerTitleFontSize,
                                    color: widget.scholarshipPageColors.textColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            if (_bannerTextOpacityAimationController.value == 0) {
                              _bannerTextOpacityAimationController.forward();
                              _bannerTextPositionAimationController.forward();
                            } else {
                              _bannerTextOpacityAimationController.reverse();
                              _bannerTextPositionAimationController.reverse();
                            }
                          },
                          child: Icon(
                            (_bannerTextOpacityAimationController.status == AnimationStatus.forward ||
                                    _bannerTextOpacityAimationController.value == 1)
                                ? Icons.keyboard_arrow_down
                                : Icons.keyboard_arrow_up,
                            size: widget.scholarshipPageStyles.bannerDescriptionFontSize + 5,
                            color: widget.scholarshipPageColors.secondaryColor,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
