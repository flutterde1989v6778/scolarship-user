import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomTextFormField extends StatelessWidget {
  CustomTextFormField({
    Key key,
    this.initialValue,
    @required this.width,
    @required this.height,
    this.fixedHeightState = false,
    this.textAlign = TextAlign.left,
    this.keyboardType = TextInputType.text,
    this.textColor = Colors.black,
    this.textFontSize = 20,
    this.hintText = "",
    this.focusNode,
    this.hintTextFontSize,
    this.hintTextColor = Colors.grey,
    this.borderRadius = 4.0,
    this.validatorHandler,
    this.onSaveHandler,
    this.borderType = 1, // 1: UnderlineInputBorder 2: OutlineInputBorder
    this.contentPadding = 5,
    this.prefixIcon,
    this.suffixIcon,
    this.borderWidth = 1,
    this.borderColor = Colors.black,
    this.enableBorderColor,
    this.focusBorderColor,
    this.errorBoarderColor = Colors.red,
    this.disabledBorderColor = const Color(0x33ffffff),
    this.fillColor = Colors.white,
    this.autovalidate = false,
    this.obscureText = false,
    this.autofocus = false,
    this.controller,
    this.onChangeHandler,
    this.onTapHandler,
    this.onFieldSubmittedHandler,
    this.maxLines = 1,
    this.textInputAction = TextInputAction.done,
    this.readOnly = false,
    this.errorStringFontSize,
    this.inputFormatters,
  }) : super(key: key);
  final String initialValue;
  double errorStringFontSize;
  final focusNode;
  final bool fixedHeightState;
  final double width;
  final double height;
  final TextAlign textAlign;
  final TextInputType keyboardType;
  final Color textColor;
  final double textFontSize;
  final String hintText;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final Color fillColor;
  final bool autovalidate;
  final bool obscureText;
  final double borderRadius;
  final Function validatorHandler;
  final Function onSaveHandler;
  final int borderType;
  final double contentPadding;
  final double borderWidth;
  final Color borderColor;
  final TextEditingController controller;
  final Function onChangeHandler;
  final Function onTapHandler;
  final Function onFieldSubmittedHandler;
  final bool autofocus;
  final int maxLines;
  final TextInputAction textInputAction;
  final bool readOnly;
  final inputFormatters;
  Color focusBorderColor;
  Color enableBorderColor;
  Color errorBoarderColor;
  Color disabledBorderColor;
  double hintTextFontSize;
  Color hintTextColor;

  @override
  Widget build(BuildContext context) {
    hintTextFontSize = hintTextFontSize ?? textFontSize;
    hintTextColor = hintTextColor ?? textColor;
    focusBorderColor = focusBorderColor ?? borderColor;
    enableBorderColor = enableBorderColor ?? borderColor;
    if (errorStringFontSize == null) errorStringFontSize = textFontSize * 0.8;

    BorderSide borderside = BorderSide(width: borderWidth, color: borderColor);
    BorderSide errorBorderside = BorderSide(width: borderWidth, color: errorBoarderColor);
    BorderSide hideBorderSide = BorderSide(width: 0, color: fillColor);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CustomTextFormFieldProvider()),
      ],
      child: Consumer<CustomTextFormFieldProvider>(
        builder: (context, customTextFormFieldProvider, _) {
          return Container(
            width: width,
            height: height + ((fixedHeightState) ? (errorStringFontSize + 5) : 0),
            alignment: Alignment.topCenter,
            child: Material(
              color: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: width,
                    height: height,
                    padding: EdgeInsets.all(1),
                    alignment: (keyboardType == TextInputType.multiline) ? Alignment.topLeft : Alignment.center,
                    decoration: (borderType == 1)
                        ? BoxDecoration(
                            color: fillColor,
                            border: Border(bottom: (customTextFormFieldProvider.errorState) ? errorBorderside : borderside),
                          )
                        : BoxDecoration(
                            color: fillColor,
                            border: (customTextFormFieldProvider.errorState)
                                ? Border.all(width: borderWidth, color: errorBoarderColor)
                                : Border.all(width: borderWidth, color: borderColor),
                            borderRadius: BorderRadius.circular(borderRadius),
                          ),
                    child: Material(
                      color: Colors.transparent,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          (prefixIcon != null)
                              ? Container(padding: EdgeInsets.only(left: contentPadding), color: fillColor, child: prefixIcon)
                              : SizedBox(),
                          Expanded(
                            child: TextFormField(
                              focusNode: focusNode,
                              maxLines: maxLines,
                              textInputAction: TextInputAction.done,
                              autofocus: autofocus,
                              initialValue: initialValue,
                              readOnly: readOnly,
                              textAlign: textAlign,
                              style: TextStyle(color: textColor, fontSize: textFontSize),
                              keyboardType: keyboardType,
                              autovalidate: autovalidate,
                              controller: controller,
                              decoration: InputDecoration(
                                errorStyle: TextStyle(fontSize: 0, color: fillColor),
                                isDense: true,
                                hintText: hintText,
                                // prefixIcon: prefixIcon,
                                // suffixIcon: suffixIcon,
                                hintStyle: TextStyle(fontSize: hintTextFontSize, color: hintTextColor),
                                border: UnderlineInputBorder(borderSide: hideBorderSide),
                                enabledBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                focusedBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                focusedErrorBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                errorBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                disabledBorder: UnderlineInputBorder(borderSide: hideBorderSide),
                                filled: true,
                                fillColor: fillColor,
                                contentPadding: EdgeInsets.symmetric(horizontal: contentPadding, vertical: contentPadding),
                                // contentPadding: EdgeInsets.symmetric(
                                //     horizontal: contentPadding, vertical: (keyboardType == TextInputType.multiline) ? contentPadding : 0),
                              ),
                              inputFormatters: inputFormatters,
                              obscureText: obscureText,
                              onTap: onTapHandler,
                              onChanged: (input) {
                                customTextFormFieldProvider.setErrorState(false, "");
                                if (onChangeHandler != null) onChangeHandler(input);
                              },
                              validator: (input) {
                                if (validatorHandler == null) return null;
                                var result = validatorHandler(input);
                                if (result != null)
                                  customTextFormFieldProvider.setErrorState(true, result);
                                else
                                  customTextFormFieldProvider.setErrorState(false, "");
                                return result;
                              },
                              onSaved: (input) {
                                if (onSaveHandler == null) return null;
                                onSaveHandler(input.trim());
                              },
                              onFieldSubmitted: onFieldSubmittedHandler,
                            ),
                          ),
                          (suffixIcon != null)
                              ? Container(padding: EdgeInsets.only(right: contentPadding), color: fillColor, child: suffixIcon)
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                  (customTextFormFieldProvider.errorState)
                      ? Container(
                          height: errorStringFontSize + 5,
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            customTextFormFieldProvider.validatorText,
                            style: TextStyle(fontSize: errorStringFontSize, color: Colors.red),
                          ),
                        )
                      : (fixedHeightState) ? SizedBox(height: errorStringFontSize) : SizedBox(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class CustomTextFormFieldProvider extends ChangeNotifier {
  static CustomTextFormFieldProvider of(BuildContext context, {bool listen = false}) =>
      Provider.of<CustomTextFormFieldProvider>(context, listen: listen);

  bool _errorState = false;
  String _validatorText = "";

  bool get errorState => _errorState;
  String get validatorText => _validatorText;

  void setErrorState(bool errorState, String validatorText) {
    if (_errorState != errorState) {
      _errorState = errorState;
      _validatorText = validatorText;
      notifyListeners();
    }
  }
}
