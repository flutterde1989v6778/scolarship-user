import 'package:flutter/material.dart';

class ScholarshipPageColors {
  Color secondaryColor;

  Color backgroundColor;
  Color textColor;
  Color panelBackgroundColor;
  LinearGradient bannerDesktopGradient;
  LinearGradient bannerMobileGradient;
  Color carouselIconColor;
  Color indicatorColor;
  Color scholarshipShadowColor;
  Color formValidatedColor;
  Color dropdownColor;
  Color cancelButtonColor;

  ScholarshipPageColors() {
    secondaryColor = Color(0xFFB58970);
    formValidatedColor = Color(0xFF2CAB00);
    dropdownColor = Color(0xFFA69D94);
    cancelButtonColor = Color(0xFFC4C4C4);
  }
}

class ScholarshipPageDarkModeColors extends ScholarshipPageColors {
  ScholarshipPageDarkModeColors() {
    backgroundColor = Colors.black;
    textColor = Colors.white;
    panelBackgroundColor = Color(0xFF232323);
    bannerDesktopGradient = const LinearGradient(
      colors: const [Color(0xFF181717), Color(0x00FFFFFF)],
      stops: const [0.0078, 0.6547],
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
    );
    bannerMobileGradient = const LinearGradient(
      colors: const [Color.fromRGBO(24, 23, 23, 0.48), Color.fromRGBO(34, 31, 31, 0)],
      stops: const [0.2599, 0.9604],
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
    );
    carouselIconColor = Color.fromRGBO(181, 137, 112, 0.15);
    indicatorColor = Colors.white;
    scholarshipShadowColor = Colors.black;
  }
}

class ScholarshipPageLightModeColors extends ScholarshipPageColors {
  ScholarshipPageLightModeColors() {
    backgroundColor = Colors.white;
    textColor = Colors.black;
    panelBackgroundColor = Colors.white;
    bannerDesktopGradient = const LinearGradient(
      colors: const [Color.fromRGBO(255, 255, 255, 0.71), Color.fromRGBO(35, 35, 35, 0)],
      stops: const [0.0078, 0.6547],
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
    );
    bannerMobileGradient = const LinearGradient(
      colors: const [Colors.white, Color.fromRGBO(255, 255, 255, 0.25)],
      stops: const [0, 0.978],
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
    );
    carouselIconColor = Color.fromRGBO(181, 137, 112, 0.15);
    indicatorColor = Color(0xFFC4C4C4);
    scholarshipShadowColor = Colors.grey[400];
  }
}
