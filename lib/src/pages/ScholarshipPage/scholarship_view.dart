import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/blocs/Blocs/ThemeBloc/index.dart';
import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/widgets/custom_raised_button.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/widgets/index.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'index.dart';

class ScholarshipView extends StatefulWidget {
  ScholarshipView({@required this.scholarshipPageColors});

  ScholarshipPageColors scholarshipPageColors;

  State<ScholarshipView> createState() => _ScholarshipViewState();
}

class _ScholarshipViewState extends State<ScholarshipView> with TickerProviderStateMixin {
  ScholarshipPageStyles _scholarshipPageStyles;
  ScholarshipPageColors _scholarshipPageColors;
  ScholarshipBloc _scholarshipBloc;

  AnimationController _selectScholarshipAnimationForDesktopController;
  Animation _selectScholarshipColorForDesktopTween;
  AnimationController _selectScholarshipAnimationForMobileController;
  Animation _selectScholarshipColorForMobileTween;

  @override
  void initState() {
    super.initState();
    _scholarshipBloc = BlocProvider.of<ScholarshipBloc>(context);

    _selectScholarshipAnimationForDesktopController = AnimationController(vsync: this);
    _selectScholarshipColorForDesktopTween =
        ColorTween(begin: Colors.transparent, end: Color(0xFF1B7FA7)).animate(_selectScholarshipAnimationForDesktopController);
    _selectScholarshipAnimationForDesktopController.addListener(() async {
      if (_selectScholarshipAnimationForDesktopController.isCompleted) {
        _selectScholarshipAnimationForDesktopController.reverse();
      }
    });
    _selectScholarshipAnimationForDesktopController.duration = Duration(milliseconds: 1);
    _selectScholarshipAnimationForDesktopController.reverseDuration = Duration(milliseconds: 500);

    _selectScholarshipAnimationForMobileController = AnimationController(vsync: this);
    _selectScholarshipColorForMobileTween =
        ColorTween(begin: Color(0xFFB58970), end: Color(0xFF1B7FA7)).animate(_selectScholarshipAnimationForMobileController);
    _selectScholarshipAnimationForMobileController.addListener(() async {
      if (_selectScholarshipAnimationForMobileController.isCompleted) {
        _selectScholarshipAnimationForMobileController.reverse();
      }
    });
    _selectScholarshipAnimationForMobileController.duration = Duration(milliseconds: 1);
    _selectScholarshipAnimationForMobileController.reverseDuration = Duration(milliseconds: 500);
  }

  @override
  void dispose() {
    super.dispose();
    _selectScholarshipAnimationForDesktopController.dispose();
    _selectScholarshipAnimationForMobileController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("___________ScholarshipView__build______________");
    _scholarshipPageColors = widget.scholarshipPageColors;
    return Material(
      color: Colors.transparent,
      child: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth >= 900) {
            _scholarshipPageStyles = ScholarshipPageDesktopStyles(context);
          } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
            ////  tablet
            _scholarshipPageStyles = ScholarshipPageTabletStyles(context);
          } else if (constraints.maxWidth < 600) {
            _scholarshipPageStyles = ScholarshipPageMobileStyles(context);
          }
          print("___ScholarshipView__LayoutBuilder___${_scholarshipPageStyles.deviceWidth}_______");

          return BlocConsumer<ScholarshipBloc, ScholarshipState>(
            listener: (context, state) async {
              if (state.submittingStateForApplication == 1) {
                await CustomProgressDialog.of(context: context).show();
              } else {
                await CustomProgressDialog.of(context: context).hide();
              }
            },
            builder: (context, state) {
              if (state.submissionSuccessViewState)
                return SubmissionSuccessPanel(
                  scholarshipPageStyles: _scholarshipPageStyles,
                  scholarshipPageColors: _scholarshipPageColors,
                  scholarshipBloc: _scholarshipBloc,
                );
              return Material(
                color: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Material(
                      color: Colors.transparent,
                      child: SingleChildScrollView(
                        child: Container(
                          width: _scholarshipPageStyles.deviceWidth,
                          color: _scholarshipPageColors.backgroundColor,
                          child: Material(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                (_scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? Material(
                                        color: Colors.transparent,
                                        child: Column(
                                          children: <Widget>[
                                            BannerForMobile(
                                              scholarshipPageStyles: _scholarshipPageStyles,
                                              scholarshipPageColors: _scholarshipPageColors,
                                            ),
                                            SizedBox(height: _scholarshipPageStyles.shareWidthDp * 30),
                                            ChoosePanelForMobile(
                                              scholarshipPageStyles: _scholarshipPageStyles,
                                              scholarshipPageColors: _scholarshipPageColors,
                                              selectScholarshipAnimationController: _selectScholarshipAnimationForMobileController,
                                              selectScholarshipColorTween: _selectScholarshipColorForMobileTween,
                                            ),
                                          ],
                                        ),
                                      )
                                    : Material(
                                        color: Colors.transparent,
                                        child: Stack(
                                          children: <Widget>[
                                            Material(
                                              color: Colors.transparent,
                                              child: Column(
                                                children: <Widget>[
                                                  BannerForDesktop(
                                                    scholarshipPageStyles: _scholarshipPageStyles,
                                                    scholarshipPageColors: _scholarshipPageColors,
                                                  ),
                                                  Container(
                                                      height: _scholarshipPageStyles.shareWidthDp * 80,
                                                      color: _scholarshipPageColors.backgroundColor),
                                                ],
                                              ),
                                            ),
                                            Positioned(
                                              bottom: 0,
                                              child: ChoosePanelForDesktop(
                                                scholarshipPageStyles: _scholarshipPageStyles,
                                                scholarshipPageColors: _scholarshipPageColors,
                                                selectScholarshipAnimationController: _selectScholarshipAnimationForDesktopController,
                                                selectScholarshipColorTween: _selectScholarshipColorForDesktopTween,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                _containerScholarshipTitle(context),
                                _containerScholarshipList(context, state),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    AdminAlert(type: 1),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }

  Widget _containerScholarshipTitle(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: _scholarshipPageStyles.opportunityHorizontalPadding,
        vertical: _scholarshipPageStyles.opportunityVerticalPadding,
      ),
      alignment: (_scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles) ? Alignment.centerLeft : Alignment.center,
      child: Text(
        (_scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
            ? "ChoosePanel.opportunityTitleForMobile".tr()
            : "ChoosePanel.opportunityTitleForDesktop".tr(),
        style: TextStyle(
          fontSize: _scholarshipPageStyles.bigTitleFontSize,
          color: _scholarshipPageColors.textColor,
        ),
      ),
    );
  }

  Widget _containerScholarshipList(BuildContext context, ScholarshipState scholarshipState) {
    if (scholarshipState.loadingStateForScholarshipList == 0) {
      _scholarshipBloc.add(LoadingScholarshipListEvent(loadingStateForScholarshipList: 1));
      _scholarshipBloc.add(GetScholarshipListStreamEvent());
    }
    return Material(
      color: Colors.transparent,
      child: StreamBuilder<List<ScholarshipModel>>(
        stream: scholarshipState.scholarshipListStream,
        builder: (context, snapshot) {
          if (scholarshipState.loadingStateForScholarshipList == -1)
            Container(
              height: _scholarshipPageStyles.shareWidthDp * 200,
              child: Center(
                child: Text(
                  "Failed. Please try it",
                  style: TextStyle(fontSize: _scholarshipPageStyles.titleFontSize, color: _scholarshipPageColors.textColor),
                ),
              ),
            );
          if (!snapshot.hasData || scholarshipState.loadingStateForScholarshipList == 1 || scholarshipState.loadingStateForScholarshipList == 0)
            return Container(
              height: _scholarshipPageStyles.shareWidthDp * 200,
              child: Center(child: CustomCupertinoIndicator(brightness: Brightness.light, size: _scholarshipPageStyles.textFontSize * 2)),
            );

          if (snapshot.hasData && (snapshot.data != null && snapshot.data.length == 0 && scholarshipState.loadingStateForScholarshipList == 2))
            return Container(
              height: _scholarshipPageStyles.shareWidthDp * 200,
              child: Center(
                child: Text(
                  "No Scholarship",
                  style: TextStyle(fontSize: _scholarshipPageStyles.titleFontSize, color: _scholarshipPageColors.textColor),
                ),
              ),
            );
          List<ScholarshipModel> _scholarshipModelList = snapshot.data;
          return LayoutBuilder(
            builder: (context, constraints) {
              return Material(
                color: Colors.transparent,
                child: Column(
                  children: _scholarshipModelList.map((scholarshipModel) {
                    return (_scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                        ? ScholarshipForMobile(
                            scholarshipPageStyles: _scholarshipPageStyles,
                            scholarshipPageColors: _scholarshipPageColors,
                            scholarshipBloc: _scholarshipBloc,
                            scholarshipState: scholarshipState,
                            scholarshipModel: scholarshipModel,
                            selectScholarshipAnimationController: _selectScholarshipAnimationForMobileController,
                            selectScholarshipColorTween: _selectScholarshipColorForMobileTween,
                          )
                        : ScholarshipForDesktop(
                            scholarshipPageStyles: _scholarshipPageStyles,
                            scholarshipPageColors: _scholarshipPageColors,
                            scholarshipBloc: _scholarshipBloc,
                            scholarshipState: scholarshipState,
                            scholarshipModel: scholarshipModel,
                            selectScholarshipAnimationController: _selectScholarshipAnimationForDesktopController,
                            selectScholarshipColorTween: _selectScholarshipColorForDesktopTween,
                          );
                  }).toList(),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
