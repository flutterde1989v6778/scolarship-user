import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/blocs/Blocs/ThemeBloc/index.dart';
import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/widgets/index.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';

class EligibilityPanelForDesktop extends StatelessWidget {
  EligibilityPanelForDesktop({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
    @required this.scholarshipState,
    @required this.scholarshipModel,
  });

  ScholarshipBloc scholarshipBloc;
  ScholarshipState scholarshipState;
  ScholarshipModel scholarshipModel;

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        width: scholarshipPageStyles.scholarshipWidth,
        padding: EdgeInsets.symmetric(
          horizontal: scholarshipPageStyles.scholarshipHorizontalPadding,
          vertical: scholarshipPageStyles.scholarshipVerticalPadding,
        ),
        color: scholarshipPageColors.panelBackgroundColor,
        child: Material(
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Material(
                color: Colors.transparent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: scholarshipPageStyles.eligibilityCardWidth,
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Eligibility.titleForDesktop".tr(),
                        style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.textColor),
                      ),
                    ),
                    Container(
                      width: scholarshipPageStyles.eligibilityCardWidth,
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Eligibility.titleForDesktop".tr(),
                        style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.panelBackgroundColor),
                      ),
                    ),
                    Container(
                      width: scholarshipPageStyles.eligibilityCardWidth,
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Eligibility.titleForDesktop".tr(),
                        style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.panelBackgroundColor),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: scholarshipPageStyles.scholarshipVerticalPadding),
              Material(
                color: Colors.transparent,
                child: BlocConsumer<ThemeBloc, ThemeState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    return Material(
                      color: Colors.transparent,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: scholarshipPageStyles.eligibilityCardWidth,
                            height: scholarshipPageStyles.eligibilityCardHeight,
                            padding: EdgeInsets.symmetric(
                              horizontal: scholarshipPageStyles.eligibilityCardHorizontalPadding,
                              vertical: scholarshipPageStyles.eligibilityCardVerticalPadding,
                            ),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: scholarshipPageColors.backgroundColor,
                              boxShadow: [
                                BoxShadow(
                                  color: (state.themeMode == ThemeModeConstants.dark)
                                      ? scholarshipPageColors.panelBackgroundColor
                                      : scholarshipPageColors.scholarshipShadowColor,
                                  offset: new Offset(0, 2),
                                  blurRadius: 2,
                                  spreadRadius: 2,
                                ),
                              ],
                            ),
                            child: Material(
                              color: Colors.transparent,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Icon(
                                          Icons.card_membership,
                                          size: scholarshipPageStyles.eligibilitySubtitleFontSize * 2,
                                          color: scholarshipPageColors.secondaryColor,
                                        )
                                      ],
                                    ),
                                  ),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            "Eligibility.subTitleForAward".tr(),
                                            style: TextStyle(
                                              fontSize: scholarshipPageStyles.eligibilitySubtitleFontSize,
                                              color: scholarshipPageColors.textColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            ScholarshipPageString.eligibilityCard1Description,
                                            style: TextStyle(
                                              fontSize: scholarshipPageStyles.textFontSize,
                                              color: scholarshipPageColors.textColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            Map<int, bool> isOpened = scholarshipState.selectEligibility[scholarshipModel.id];
                                            if (!isOpened[0]) {
                                              isOpened[0] = true;
                                              isOpened[1] = false;
                                              isOpened[2] = false;
                                            } else {
                                              isOpened[0] = false;
                                              isOpened[1] = false;
                                              isOpened[2] = false;
                                            }
                                            scholarshipBloc.add(SelectEligibilityEvent(id: scholarshipModel.id, isOpened: isOpened));
                                          },
                                          child: Material(
                                            color: Colors.transparent,
                                            child: Row(
                                              children: <Widget>[
                                                Text(
                                                  "Eligibility.details".tr(),
                                                  style: TextStyle(
                                                    fontSize: scholarshipPageStyles.textFontSize,
                                                    color: scholarshipPageColors.textColor,
                                                  ),
                                                ),
                                                SizedBox(width: 5),
                                                Icon(
                                                  (!scholarshipState.selectEligibility[scholarshipModel.id][0])
                                                      ? Icons.keyboard_arrow_down
                                                      : Icons.keyboard_arrow_up,
                                                  size: scholarshipPageStyles.textFontSize * 1.5,
                                                  color: scholarshipPageColors.secondaryColor,
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: scholarshipPageStyles.eligibilityCardWidth,
                            height: scholarshipPageStyles.eligibilityCardHeight,
                            padding: EdgeInsets.symmetric(
                              horizontal: scholarshipPageStyles.eligibilityCardHorizontalPadding,
                              vertical: scholarshipPageStyles.eligibilityCardVerticalPadding,
                            ),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: scholarshipPageColors.backgroundColor,
                              boxShadow: [
                                BoxShadow(
                                  color: (state.themeMode == ThemeModeConstants.dark)
                                      ? scholarshipPageColors.panelBackgroundColor
                                      : scholarshipPageColors.scholarshipShadowColor,
                                  offset: new Offset(0, 2),
                                  blurRadius: 2,
                                  spreadRadius: 2,
                                ),
                              ],
                            ),
                            child: Material(
                              color: Colors.transparent,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Icon(
                                          Icons.playlist_add_check,
                                          size: scholarshipPageStyles.eligibilitySubtitleFontSize * 2,
                                          color: scholarshipPageColors.secondaryColor,
                                        )
                                      ],
                                    ),
                                  ),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            "Eligibility.subTitleForRequirements".tr(),
                                            style: TextStyle(
                                              fontSize: scholarshipPageStyles.eligibilitySubtitleFontSize,
                                              color: scholarshipPageColors.textColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            ScholarshipPageString.eligibilityCard2Description,
                                            style: TextStyle(
                                              fontSize: scholarshipPageStyles.textFontSize,
                                              color: scholarshipPageColors.textColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            Map<int, bool> isOpened = scholarshipState.selectEligibility[scholarshipModel.id];
                                            if (!isOpened[1]) {
                                              isOpened[0] = false;
                                              isOpened[1] = true;
                                              isOpened[2] = false;
                                            } else {
                                              isOpened[0] = false;
                                              isOpened[1] = false;
                                              isOpened[2] = false;
                                            }
                                            scholarshipBloc.add(SelectEligibilityEvent(id: scholarshipModel.id, isOpened: isOpened));
                                          },
                                          child: Material(
                                            color: Colors.transparent,
                                            child: Row(
                                              children: <Widget>[
                                                Text(
                                                  "Eligibility.details".tr(),
                                                  style: TextStyle(
                                                    fontSize: scholarshipPageStyles.textFontSize,
                                                    color: scholarshipPageColors.textColor,
                                                  ),
                                                ),
                                                SizedBox(width: 5),
                                                Icon(
                                                  (!scholarshipState.selectEligibility[scholarshipModel.id][1])
                                                      ? Icons.keyboard_arrow_down
                                                      : Icons.keyboard_arrow_up,
                                                  size: scholarshipPageStyles.textFontSize * 1.5,
                                                  color: scholarshipPageColors.secondaryColor,
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: scholarshipPageStyles.eligibilityCardWidth,
                            height: scholarshipPageStyles.eligibilityCardHeight,
                            padding: EdgeInsets.symmetric(
                              horizontal: scholarshipPageStyles.eligibilityCardHorizontalPadding,
                              vertical: scholarshipPageStyles.eligibilityCardVerticalPadding,
                            ),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: scholarshipPageColors.backgroundColor,
                              boxShadow: [
                                BoxShadow(
                                  color: (state.themeMode == ThemeModeConstants.dark)
                                      ? scholarshipPageColors.panelBackgroundColor
                                      : scholarshipPageColors.scholarshipShadowColor,
                                  offset: new Offset(0, 2),
                                  blurRadius: 2,
                                  spreadRadius: 2,
                                ),
                              ],
                            ),
                            child: Material(
                              color: Colors.transparent,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Icon(
                                          Icons.event_note,
                                          size: scholarshipPageStyles.eligibilitySubtitleFontSize * 2,
                                          color: scholarshipPageColors.secondaryColor,
                                        )
                                      ],
                                    ),
                                  ),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            "Eligibility.subTitleForDateTimes".tr(),
                                            style: TextStyle(
                                              fontSize: scholarshipPageStyles.eligibilitySubtitleFontSize,
                                              color: scholarshipPageColors.textColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            ScholarshipPageString.eligibilityCard3Description,
                                            style: TextStyle(
                                              fontSize: scholarshipPageStyles.textFontSize,
                                              color: scholarshipPageColors.textColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                  Material(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: () {
                                            Map<int, bool> isOpened = scholarshipState.selectEligibility[scholarshipModel.id];
                                            if (!isOpened[2]) {
                                              isOpened[0] = false;
                                              isOpened[1] = false;
                                              isOpened[2] = true;
                                            } else {
                                              isOpened[0] = false;
                                              isOpened[1] = false;
                                              isOpened[2] = false;
                                            }
                                            scholarshipBloc.add(SelectEligibilityEvent(id: scholarshipModel.id, isOpened: isOpened));
                                          },
                                          child: Material(
                                            color: Colors.transparent,
                                            child: Row(
                                              children: <Widget>[
                                                Text(
                                                  "Eligibility.details".tr(),
                                                  style: TextStyle(
                                                    fontSize: scholarshipPageStyles.textFontSize,
                                                    color: scholarshipPageColors.textColor,
                                                  ),
                                                ),
                                                SizedBox(width: 5),
                                                Icon(
                                                  (!scholarshipState.selectEligibility[scholarshipModel.id][2])
                                                      ? Icons.keyboard_arrow_down
                                                      : Icons.keyboard_arrow_up,
                                                  size: scholarshipPageStyles.textFontSize * 1.5,
                                                  color: scholarshipPageColors.secondaryColor,
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(height: scholarshipPageStyles.scholarshipVerticalPadding),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: scholarshipPageStyles.shareWidthDp,
                      color: ((scholarshipState.selectEligibility[scholarshipModel.id][0]) ||
                              (scholarshipState.selectEligibility[scholarshipModel.id][1]) ||
                              (scholarshipState.selectEligibility[scholarshipModel.id][2]))
                          ? scholarshipPageColors.secondaryColor
                          : scholarshipPageColors.panelBackgroundColor,
                    ),
                  ),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: scholarshipPageStyles.eligibilityCardWidth,
                        height: scholarshipPageStyles.lineHeight,
                        color: (scholarshipState.selectEligibility[scholarshipModel.id][0])
                            ? scholarshipPageColors.secondaryColor
                            : scholarshipPageColors.panelBackgroundColor,
                      ),
                      Container(
                        width: scholarshipPageStyles.eligibilityCardWidth,
                        height: scholarshipPageStyles.lineHeight,
                        color: (scholarshipState.selectEligibility[scholarshipModel.id][1])
                            ? scholarshipPageColors.secondaryColor
                            : scholarshipPageColors.panelBackgroundColor,
                      ),
                      Container(
                        width: scholarshipPageStyles.eligibilityCardWidth,
                        height: scholarshipPageStyles.lineHeight,
                        color: (scholarshipState.selectEligibility[scholarshipModel.id][2])
                            ? scholarshipPageColors.secondaryColor
                            : scholarshipPageColors.panelBackgroundColor,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
              (scholarshipState.selectEligibility[scholarshipModel.id][0])
                  ? Material(
                      color: Colors.transparent,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            ScholarshipPageString.eligibilitySubTitle1,
                            style: TextStyle(fontSize: scholarshipPageStyles.eligibilitySubtitleFontSize, color: scholarshipPageColors.textColor),
                          ),
                          SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                          Text(
                            ScholarshipPageString.eligibilityDescription1,
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.eligibilitySubtitleFontSize,
                              color: scholarshipPageColors.textColor,
                              height: 1.5,
                              letterSpacing: 1.5,
                            ),
                          ),
                        ],
                      ),
                    )
                  : SizedBox(),
              (scholarshipState.selectEligibility[scholarshipModel.id][1])
                  ? Material(
                      color: Colors.transparent,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            ScholarshipPageString.eligibilitySubTitle2,
                            style: TextStyle(fontSize: scholarshipPageStyles.eligibilitySubtitleFontSize, color: scholarshipPageColors.textColor),
                          ),
                          SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                          Text(
                            ScholarshipPageString.eligibilityDescription2,
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.textFontSize,
                              color: scholarshipPageColors.textColor,
                              height: 1.5,
                              letterSpacing: 1.5,
                            ),
                          ),
                        ],
                      ),
                    )
                  : SizedBox(),
              (scholarshipState.selectEligibility[scholarshipModel.id][2])
                  ? Material(
                      color: Colors.transparent,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            ScholarshipPageString.eligibilitySubTitle3,
                            style: TextStyle(fontSize: scholarshipPageStyles.eligibilitySubtitleFontSize, color: scholarshipPageColors.textColor),
                          ),
                          SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                          Text(
                            ScholarshipPageString.eligibilityDescription3,
                            style: TextStyle(
                              fontSize: scholarshipPageStyles.textFontSize,
                              color: scholarshipPageColors.textColor,
                              height: 1.5,
                              letterSpacing: 1.5,
                            ),
                          ),
                        ],
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
