import 'dart:math';

import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import "package:universal_html/html.dart" as html;
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scholarship/src/widgets/index.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';

class SubmissionSuccessPanel extends StatelessWidget {
  SubmissionSuccessPanel({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
  });
  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  ScholarshipBloc scholarshipBloc;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: scholarshipPageColors.backgroundColor,
        height: scholarshipPageStyles.shareWidthDp * 300,
        alignment: Alignment.center,
        child: Center(
          child: Container(
            height: scholarshipPageStyles.shareWidthDp * 300,
            margin: EdgeInsets.symmetric(
              horizontal: scholarshipPageStyles.scholarshipHorizontalMargin,
              vertical: scholarshipPageStyles.scholarshipVerticalMargin * 2,
            ),
            padding: EdgeInsets.symmetric(
              horizontal: scholarshipPageStyles.scholarshipHorizontalPadding,
              vertical: scholarshipPageStyles.scholarshipVerticalPadding,
            ),
            decoration: BoxDecoration(
              border: Border.all(color: scholarshipPageColors.secondaryColor),
              color: scholarshipPageColors.backgroundColor,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "SubmissionSuccessPanel.title".tr(),
                  style: TextStyle(
                    fontSize: scholarshipPageStyles.bigTitleFontSize,
                    color: scholarshipPageColors.textColor,
                  ),
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 15),
                GestureDetector(
                  onTap: () {
                    scholarshipBloc.add(ReturnScholarshipPageEvent());
                  },
                  child: Text(
                    "SubmissionSuccessPanel.link".tr(),
                    style: TextStyle(
                      fontSize: scholarshipPageStyles.textFontSize + 2,
                      color: scholarshipPageColors.secondaryColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
