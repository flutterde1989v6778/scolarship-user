class ScholarshipApplicationModel {
  String id;
  String scholarshipID;
  String name;
  String email;
  dynamic discipline;
  String phoneNumber;
  String location;
  int age;
  String twitter;
  String instagram;
  String facebook;
  String videoUrl;
  String letter1Url;
  String letter2Url;
  int ts;

  ScholarshipApplicationModel({
    id = "",
    scholarshipID = "",
    name = "",
    email = "",
    discipline = "",
    phoneNumber = "",
    location = "",
    isOpend = false,
    age = 0,
    twitter = "",
    instagram = "",
    facebook = "",
    videoUrl = "",
    letter1Url = "",
    letter2Url = "",
    ts = 0,
  }) {
    this.id = id;
    this.scholarshipID = scholarshipID;
    this.name = name;
    this.email = email;
    this.discipline = discipline;
    this.phoneNumber = phoneNumber;
    this.location = location;
    this.age = age;
    this.twitter = twitter;
    this.instagram = instagram;
    this.facebook = facebook;
    this.videoUrl = videoUrl;
    this.letter1Url = letter1Url;
    this.letter2Url = letter2Url;
    this.ts = ts;
  }

  ScholarshipApplicationModel.fromJson(Map<String, dynamic> json)
      : id = (json['id'] != null) ? json['id'] : "",
        scholarshipID = (json['scholarshipID'] != null) ? json['scholarshipID'] : "",
        name = (json['name'] != null) ? json['name'] : "",
        email = (json['email'] != null) ? json['email'] : "",
        discipline = (json['discipline'] != null) ? json['discipline'] : "",
        phoneNumber = (json['phoneNumber'] != null) ? json['phoneNumber'] : "",
        location = (json['location'] != null) ? json['location'] : "",
        age = (json['age'] != null) ? json['age'] : 0,
        twitter = (json['twitter'] != null) ? json['twitter'] : "",
        instagram = (json['instagram'] != null) ? json['instagram'] : "",
        facebook = (json['facebook'] != null) ? json['facebook'] : "",
        videoUrl = (json['videoUrl'] != null) ? json['videoUrl'] : "",
        letter1Url = (json['letter1Url'] != null) ? json['letter1Url'] : "",
        letter2Url = (json['letter2Url'] != null) ? json['letter2Url'] : "",
        ts = (json['ts'] != null) ? json['ts'] : "";

  Map<String, dynamic> toJson() {
    return {
      "id": (id == null) ? "" : id,
      "scholarshipID": (scholarshipID == null) ? "" : scholarshipID,
      "name": (name == null) ? "" : name,
      "email": (email == null) ? "" : email,
      "discipline": (discipline == null) ? "" : discipline,
      "phoneNumber": (phoneNumber == null) ? "" : phoneNumber,
      "location": (location == null) ? "" : location,
      "age": (age == null) ? 0 : age,
      "twitter": (twitter == null) ? "" : twitter,
      "instagram": (instagram == null) ? "" : instagram,
      "facebook": (facebook == null) ? "" : facebook,
      "videoUrl": (videoUrl == null) ? "" : videoUrl,
      "letter1Url": (letter1Url == null) ? "" : letter1Url,
      "letter2Url": (letter2Url == null) ? "" : letter2Url,
      "ts": (ts == null) ? "" : ts,
    };
  }
}
