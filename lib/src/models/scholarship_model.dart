import 'index.dart';

class ScholarshipModel extends BaseModel {
  String id;
  String title;
  String description;
  String imageUrl;
  bool isOpened;
  int deadlines;

  /// about
  String videoUrlForAbout;
  String subTitle1ForAbout;
  String subDescription1ForAbout;
  String subTitle2ForAbout;
  String subDescription2ForAbout;

  /// eligibility
  String descriptionOfAwardSectionForEligibility;
  String detailTitleOfAwardSectionForEligibility;
  String detailDescriptionOfAwardSectionForEligibility;

  String descriptionOfRequirementSectionForEligibility;
  String detailTitleOfRequirementSectionForEligibility;
  String detailDescriptionOfRequirementSectionForEligibility;

  String descriptionOfDateTimesForEligibility;
  String detailTitleOfDateTimesForEligibility;
  String detailDescriptionOfDateTimesForEligibility;
  int ts;

  ScholarshipModel({
    id = "",
    title = "",
    description = "",
    imageUrl = "",
    isOpened = false,
    deadlines = 0,
    videoUrlForAbout = "",
    subTitle1ForAbout = "",
    subDescription1ForAbout = "",
    subTitle2ForAbout = "",
    subDescription2ForAbout = "",
    descriptionOfAwardSectionForEligibility = "",
    detailTitleOfAwardSectionForEligibility = "",
    detailDescriptionOfAwardSectionForEligibility = "",
    descriptionOfRequirementSectionForEligibility = "",
    detailTitleOfRequirementSectionForEligibility = "",
    detailDescriptionOfRequirementSectionForEligibility = "",
    descriptionOfDateTimesForEligibility = "",
    detailTitleOfDateTimesForEligibility = "",
    detailDescriptionOfDateTimesForEligibility = "",
    ts = 0,
  }) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.imageUrl = imageUrl;
    this.isOpened = isOpened;
    this.deadlines = deadlines;
    this.videoUrlForAbout = videoUrlForAbout;
    this.subTitle1ForAbout = subTitle1ForAbout;
    this.subDescription1ForAbout = subDescription1ForAbout;
    this.subTitle2ForAbout = subTitle2ForAbout;
    this.subDescription2ForAbout = subDescription2ForAbout;
    this.descriptionOfAwardSectionForEligibility = descriptionOfAwardSectionForEligibility;
    this.detailTitleOfAwardSectionForEligibility = detailTitleOfAwardSectionForEligibility;
    this.detailDescriptionOfAwardSectionForEligibility = detailDescriptionOfAwardSectionForEligibility;
    this.descriptionOfRequirementSectionForEligibility = descriptionOfRequirementSectionForEligibility;
    this.detailTitleOfRequirementSectionForEligibility = detailTitleOfRequirementSectionForEligibility;
    this.detailDescriptionOfRequirementSectionForEligibility = detailDescriptionOfRequirementSectionForEligibility;
    this.descriptionOfDateTimesForEligibility = descriptionOfDateTimesForEligibility;
    this.detailTitleOfDateTimesForEligibility = detailTitleOfDateTimesForEligibility;
    this.detailDescriptionOfDateTimesForEligibility = detailDescriptionOfDateTimesForEligibility;
    this.ts = ts;
  }

  ScholarshipModel.fromJson(Map<String, dynamic> json)
      : id = (json['id'] != null) ? json['id'] : "",
        title = (json['title'] != null) ? json['title'] : "",
        description = (json['description'] != null) ? json['description'] : "",
        imageUrl = (json['imageUrl'] != null) ? json['imageUrl'] : "",
        isOpened = (json['isOpened'] != null) ? json['isOpened'] : false,
        deadlines = (json['deadlines'] != null) ? json['deadlines'] : 0,
        videoUrlForAbout = (json['videoUrlForAbout'] != null) ? json['videoUrlForAbout'] : "",
        subTitle1ForAbout = (json['subTitle1ForAbout'] != null) ? json['subTitle1ForAbout'] : "",
        subDescription1ForAbout = (json['subDescription1ForAbout'] != null) ? json['subDescription1ForAbout'] : "",
        subTitle2ForAbout = (json['subTitle2ForAbout'] != null) ? json['subTitle2ForAbout'] : "",
        subDescription2ForAbout = (json['subDescription2ForAbout'] != null) ? json['subDescription2ForAbout'] : "",
        descriptionOfAwardSectionForEligibility =
            (json['descriptionOfAwardSectionForEligibility'] != null) ? json['descriptionOfAwardSectionForEligibility'] : "",
        detailTitleOfAwardSectionForEligibility =
            (json['detailTitleOfAwardSectionForEligibility'] != null) ? json['detailTitleOfAwardSectionForEligibility'] : "",
        detailDescriptionOfAwardSectionForEligibility =
            (json['detailDescriptionOfAwardSectionForEligibility'] != null) ? json['detailDescriptionOfAwardSectionForEligibility'] : "",
        descriptionOfRequirementSectionForEligibility =
            (json['descriptionOfRequirementSectionForEligibility'] != null) ? json['descriptionOfRequirementSectionForEligibility'] : "",
        detailTitleOfRequirementSectionForEligibility =
            (json['detailTitleOfRequirementSectionForEligibility'] != null) ? json['detailTitleOfRequirementSectionForEligibility'] : "",
        detailDescriptionOfRequirementSectionForEligibility =
            (json['detailDescriptionOfRequirementSectionForEligibility'] != null) ? json['detailDescriptionOfRequirementSectionForEligibility'] : "",
        descriptionOfDateTimesForEligibility =
            (json['descriptionOfDateTimesForEligibility'] != null) ? json['descriptionOfDateTimesForEligibility'] : "",
        detailTitleOfDateTimesForEligibility =
            (json['detailTitleOfDateTimesForEligibility'] != null) ? json['detailTitleOfDateTimesForEligibility'] : "",
        detailDescriptionOfDateTimesForEligibility =
            (json['detailDescriptionOfDateTimesForEligibility'] != null) ? json['detailDescriptionOfDateTimesForEligibility'] : "",
        ts = (json['ts'] != null) ? json['ts'] : 0;

  Map<String, dynamic> toJson() {
    return {
      "id": (id == null) ? "" : id,
      "title": (title == null) ? "" : title,
      "description": (description == null) ? "" : description,
      "imageUrl": (imageUrl == null) ? "" : imageUrl,
      "isOpened": (isOpened == null) ? false : isOpened,
      "deadlines": (deadlines == null) ? 0 : deadlines,
      "videoUrlForAbout": (videoUrlForAbout == null) ? "" : videoUrlForAbout,
      "subTitle1ForAbout": (subTitle1ForAbout == null) ? "" : subTitle1ForAbout,
      "subDescription1ForAbout": (subDescription1ForAbout == null) ? "" : subDescription1ForAbout,
      "subTitle2ForAbout": (subTitle2ForAbout == null) ? "" : subTitle2ForAbout,
      "subDescription2ForAbout": (subDescription2ForAbout == null) ? "" : subDescription2ForAbout,
      "descriptionOfAwardSectionForEligibility": (descriptionOfAwardSectionForEligibility == null) ? "" : descriptionOfAwardSectionForEligibility,
      "detailTitleOfAwardSectionForEligibility": (detailTitleOfAwardSectionForEligibility == null) ? "" : detailTitleOfAwardSectionForEligibility,
      "detailDescriptionOfAwardSectionForEligibility":
          (detailDescriptionOfAwardSectionForEligibility == null) ? "" : detailDescriptionOfAwardSectionForEligibility,
      "descriptionOfRequirementSectionForEligibility":
          (descriptionOfRequirementSectionForEligibility == null) ? "" : descriptionOfRequirementSectionForEligibility,
      "detailTitleOfRequirementSectionForEligibility":
          (detailTitleOfRequirementSectionForEligibility == null) ? "" : detailTitleOfRequirementSectionForEligibility,
      "detailDescriptionOfRequirementSectionForEligibility":
          (detailDescriptionOfRequirementSectionForEligibility == null) ? "" : detailDescriptionOfRequirementSectionForEligibility,
      "descriptionOfDateTimesForEligibility": (descriptionOfDateTimesForEligibility == null) ? "" : descriptionOfDateTimesForEligibility,
      "detailTitleOfDateTimesForEligibility": (detailTitleOfDateTimesForEligibility == null) ? "" : detailTitleOfDateTimesForEligibility,
      "detailDescriptionOfDateTimesForEligibility":
          (detailDescriptionOfDateTimesForEligibility == null) ? "" : detailDescriptionOfDateTimesForEligibility,
      "ts": (ts == null) ? 0 : ts,
    };
  }
}
