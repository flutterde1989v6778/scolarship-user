import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AdminAlertProvider extends ChangeNotifier {
  static AdminAlertProvider of(BuildContext context, {bool listen = false}) => Provider.of<AdminAlertProvider>(context, listen: listen);

  bool _isShown = true;
  bool get isShown => _isShown;
  void setIsShown(bool isShown) {
    if (_isShown != isShown) {
      _isShown = isShown;
      notifyListeners();
    }
  }
}
