import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/widgets/index.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Components/index.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class ScholarshipNavigatorForMobile extends StatelessWidget {
  ScholarshipNavigatorForMobile({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
    @required this.scholarshipState,
    @required this.scholarshipModel,
  });

  ScholarshipBloc scholarshipBloc;
  ScholarshipState scholarshipState;
  ScholarshipModel scholarshipModel;

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  PageController carouselController = PageController(viewportFraction: 0.8);

  @override
  Widget build(BuildContext context) {
    carouselController.addListener(() {
      if (carouselController.page == 0)
        scholarshipBloc.add(SelectDetailTabEvent(id: scholarshipModel.id, tapIndex: 1));
      else if (carouselController.page == 1)
        scholarshipBloc.add(SelectDetailTabEvent(id: scholarshipModel.id, tapIndex: 2));
      else if (carouselController.page == 2) scholarshipBloc.add(SelectDetailTabEvent(id: scholarshipModel.id, tapIndex: 3));
    });
    return Material(
      color: Colors.transparent,
      child: Container(
        width: scholarshipPageStyles.deviceWidth,
        child: Material(
          color: Colors.transparent,
          child: Column(
            children: <Widget>[
              SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
              SizedBox(
                height: scholarshipPageStyles.carouselHeight,
                child: PageView(
                  controller: carouselController,
                  children: <Widget>[
                    Container(
                      width: scholarshipPageStyles.carouselWidth,
                      height: scholarshipPageStyles.carouselHeight,
                      margin: EdgeInsets.only(right: scholarshipPageStyles.carouselMargin),
                      decoration: BoxDecoration(
                        color: scholarshipPageColors.panelBackgroundColor,
                        boxShadow: [
                          BoxShadow(
                            color: scholarshipPageColors.scholarshipShadowColor,
                            offset: new Offset(0, 2),
                            blurRadius: 2,
                            spreadRadius: 2,
                          ),
                        ],
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: scholarshipPageStyles.carouselWidth,
                              padding: EdgeInsets.all(scholarshipPageStyles.shareWidthDp * 10),
                              alignment: Alignment.topRight,
                              child: Text(
                                "1",
                                style: TextStyle(fontSize: 80, color: scholarshipPageColors.carouselIconColor, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              width: scholarshipPageStyles.carouselWidth,
                              height: scholarshipPageStyles.carouselHeight,
                              margin: EdgeInsets.only(right: scholarshipPageStyles.carouselMargin),
                              padding: EdgeInsets.symmetric(
                                horizontal: scholarshipPageStyles.choosePanelHorizontalPadding,
                                vertical: scholarshipPageStyles.choosePanelVerticalPadding,
                              ),
                              alignment: Alignment.center,
                              child: Material(
                                color: Colors.transparent,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "ScholarshipNavigatorForMobile.scholarship".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.scholarshipTitleFontSize,
                                        color: scholarshipPageColors.secondaryColor.withAlpha(128),
                                      ),
                                    ),
                                    SizedBox(height: scholarshipPageStyles.shareWidthDp * 5),
                                    Text(
                                      "ScholarshipNavigatorForMobile.menu1".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.scholarshipTitleFontSize,
                                        color: scholarshipPageColors.secondaryColor,
                                      ),
                                    ),
                                    SizedBox(height: scholarshipPageStyles.shareWidthDp * 15),
                                    Text(
                                      "ScholarshipNavigatorForMobile.description1".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.titleFontSize,
                                        color: scholarshipPageColors.textColor,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: scholarshipPageStyles.carouselWidth,
                      height: scholarshipPageStyles.carouselHeight,
                      margin: EdgeInsets.symmetric(horizontal: scholarshipPageStyles.carouselMargin),
                      decoration: BoxDecoration(
                        color: scholarshipPageColors.panelBackgroundColor,
                        boxShadow: [
                          BoxShadow(
                            color: scholarshipPageColors.scholarshipShadowColor,
                            offset: new Offset(0, 2),
                            blurRadius: 2,
                            spreadRadius: 2,
                          ),
                        ],
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: scholarshipPageStyles.carouselWidth,
                              padding: EdgeInsets.all(10),
                              alignment: Alignment.topRight,
                              child: Text(
                                "2",
                                style: TextStyle(fontSize: 80, color: scholarshipPageColors.carouselIconColor, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              width: scholarshipPageStyles.carouselWidth,
                              height: scholarshipPageStyles.carouselHeight,
                              padding: EdgeInsets.symmetric(
                                horizontal: scholarshipPageStyles.choosePanelHorizontalPadding,
                                vertical: scholarshipPageStyles.choosePanelVerticalPadding,
                              ),
                              alignment: Alignment.center,
                              child: Material(
                                color: Colors.transparent,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "ScholarshipNavigatorForMobile.scholarship".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.scholarshipTitleFontSize,
                                        color: scholarshipPageColors.secondaryColor.withAlpha(128),
                                      ),
                                    ),
                                    SizedBox(height: scholarshipPageStyles.shareWidthDp * 5),
                                    Text(
                                      "ScholarshipNavigatorForMobile.menu2".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.scholarshipTitleFontSize,
                                        color: scholarshipPageColors.secondaryColor,
                                      ),
                                    ),
                                    SizedBox(height: scholarshipPageStyles.shareWidthDp * 15),
                                    Text(
                                      "ScholarshipNavigatorForMobile.description2".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.titleFontSize,
                                        color: scholarshipPageColors.textColor,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: scholarshipPageStyles.carouselWidth,
                      height: scholarshipPageStyles.carouselHeight,
                      margin: EdgeInsets.only(left: scholarshipPageStyles.carouselMargin),
                      decoration: BoxDecoration(
                        color: scholarshipPageColors.panelBackgroundColor,
                        boxShadow: [
                          BoxShadow(
                            color: scholarshipPageColors.scholarshipShadowColor,
                            offset: new Offset(0, 2),
                            blurRadius: 2,
                            spreadRadius: 2,
                          ),
                        ],
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: scholarshipPageStyles.carouselWidth,
                              height: scholarshipPageStyles.carouselHeight,
                              padding: EdgeInsets.all(10),
                              alignment: Alignment.topRight,
                              child: Text(
                                "3",
                                style: TextStyle(fontSize: 80, color: scholarshipPageColors.carouselIconColor, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              width: scholarshipPageStyles.carouselWidth,
                              height: scholarshipPageStyles.carouselHeight,
                              padding: EdgeInsets.symmetric(
                                horizontal: scholarshipPageStyles.choosePanelHorizontalPadding,
                                vertical: scholarshipPageStyles.choosePanelVerticalPadding,
                              ),
                              alignment: Alignment.center,
                              child: Material(
                                color: Colors.transparent,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "ScholarshipNavigatorForMobile.scholarship".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.scholarshipTitleFontSize,
                                        color: scholarshipPageColors.secondaryColor.withAlpha(128),
                                      ),
                                    ),
                                    SizedBox(height: scholarshipPageStyles.shareWidthDp * 5),
                                    Text(
                                      "ScholarshipNavigatorForMobile.menu3".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.titleFontSize,
                                        color: scholarshipPageColors.secondaryColor,
                                      ),
                                    ),
                                    SizedBox(height: scholarshipPageStyles.shareWidthDp * 15),
                                    Text(
                                      "ScholarshipNavigatorForMobile.description3".tr(),
                                      style: TextStyle(
                                        fontSize: scholarshipPageStyles.titleFontSize,
                                        color: scholarshipPageColors.textColor,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                alignment: Alignment.center,
                child: SmoothPageIndicator(
                  controller: carouselController,
                  count: 3,
                  effect: ExpandingDotsEffect(
                    expansionFactor: 3,
                    dotColor: scholarshipPageColors.indicatorColor,
                    activeDotColor: scholarshipPageColors.indicatorColor,
                    dotWidth: 6,
                    dotHeight: 6,
                    radius: 3,
                  ),
                ),
              ),
              (scholarshipState.selectDetailTab[scholarshipModel.id] == 1)
                  ? AboutUsPanel(
                      scholarshipPageStyles: scholarshipPageStyles,
                      scholarshipPageColors: scholarshipPageColors,
                      scholarshipBloc: scholarshipBloc,
                      scholarshipState: scholarshipState,
                      scholarshipModel: scholarshipModel,
                    )
                  : (scholarshipState.selectDetailTab[scholarshipModel.id] == 2)
                      ? (EligibilityPanelForMobile(
                          scholarshipPageStyles: scholarshipPageStyles,
                          scholarshipPageColors: scholarshipPageColors,
                          scholarshipBloc: scholarshipBloc,
                          scholarshipState: scholarshipState,
                          scholarshipModel: scholarshipModel,
                        ))
                      : SubmitApplicationPanel(
                          scholarshipPageStyles: scholarshipPageStyles,
                          scholarshipPageColors: scholarshipPageColors,
                          scholarshipBloc: scholarshipBloc,
                          scholarshipState: scholarshipState,
                          scholarshipModel: scholarshipModel,
                        ),
            ],
          ),
        ),
      ),
    );
  }
}
