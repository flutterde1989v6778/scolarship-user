import 'package:flutter/material.dart';
import 'package:scholarship/src/models/index.dart';
import 'package:scholarship/src/providers/index.dart';

import 'index.dart';

class ScholarshipApplicationRepository {
  ScholarshipApplicationProvider scholarshipProvider = ScholarshipApplicationProvider();

  Future<ScholarshipApplicationModel> addScholarshipApplication({@required ScholarshipApplicationModel scholarshipApplicationModel}) async {
    return await scholarshipProvider.addScholarshipApplication(scholarshipApplicationModel: scholarshipApplicationModel);
  }

  Future<bool> updateScholarshipApplication({@required ScholarshipApplicationModel scholarshipApplicationModel}) async {
    return await scholarshipProvider.updateScholarshipApplication(scholarshipApplicationModel: scholarshipApplicationModel);
  }

  Future<dynamic> deleteScholarshipApplication({@required ScholarshipApplicationModel scholarshipApplicationModel}) async {
    return await scholarshipProvider.deleteScholarshipApplication(scholarshipApplicationModel: scholarshipApplicationModel);
  }

  Future<List<ScholarshipApplicationModel>> getScholarshipApplicationList({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) async {
    try {
      List<ScholarshipApplicationModel> list =
          await scholarshipProvider.getScholarshipApplicationList(wheres: wheres, orderby: orderby, limit: limit);
      return list;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Stream<List<ScholarshipApplicationModel>> getScholarshipApplicationListStream({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) {
    return scholarshipProvider.getScholarshipApplicationListStream(wheres: wheres, orderby: orderby, limit: limit);
  }
}
