import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import "package:universal_html/html.dart" as html;
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/widgets/index.dart';
import 'package:scholarship/src/models/scholarship_application_model.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';

class SubmitApplicationPanel extends StatelessWidget {
  SubmitApplicationPanel({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
    @required this.scholarshipState,
    @required this.scholarshipModel,
  });
  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;
  ScholarshipBloc scholarshipBloc;
  ScholarshipState scholarshipState;
  ScholarshipModel scholarshipModel;

  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  Map<String, dynamic> _scholarshipApplicationData;
  bool _applicationValdate = false;

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> ageItems = [];
    for (var i = 25; i <= 100; i++) {
      ageItems.add({"value": i, "text": i.toString()});
    }

    _scholarshipApplicationData = scholarshipState.scholarshipApplicationData[scholarshipModel.id] ?? Map<String, dynamic>();

    if (_scholarshipApplicationData["appplicationModel"] == null) _scholarshipApplicationData["appplicationModel"] = ScholarshipApplicationModel();
    if (_scholarshipApplicationData["formValidate"] == null) _scholarshipApplicationData["formValidate"] = Map<String, bool>();

    _applicationValdate = _formValidator();

    return Material(
      color: Colors.transparent,
      child: Container(
        width: scholarshipPageStyles.scholarshipWidth,
        padding: EdgeInsets.symmetric(
          horizontal: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
              ? scholarshipPageStyles.scholarshipHorizontalPadding
              : scholarshipPageStyles.scholarshipHorizontalPadding * 2,
          vertical: scholarshipPageStyles.scholarshipVerticalPadding,
        ),
        color: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
            ? scholarshipPageColors.backgroundColor
            : scholarshipPageColors.panelBackgroundColor,
        child: Form(
          key: _formkey,
          child: Material(
            color: Colors.transparent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
                Text(
                  "Eligibility.titleForDesktop".tr(),
                  style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.textColor),
                ),
                Text(
                  "SubmitApplication.required".tr(),
                  style: TextStyle(fontSize: scholarshipPageStyles.textFontSize * 0.8, color: Colors.red),
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
                //// ---- Name ---- ////
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["name"] != null && _scholarshipApplicationData["formValidate"]["name"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.nameTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomTextFormField(
                                initialValue: _scholarshipApplicationData["appplicationModel"].name,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                borderWidth: 1,
                                hintText: "SubmitApplication.nameTextFieldHintText".tr(),
                                hintTextColor: Colors.grey,
                                hintTextFontSize: scholarshipPageStyles.textFontSize,
                                textFontSize: scholarshipPageStyles.textFontSize,
                                textColor: scholarshipPageColors.textColor,
                                fixedHeightState: true,
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                onChangeHandler: (input) => _scholarshipApplicationData["appplicationModel"].name = input.trim(),
                                // validatorHandler: (input) {
                                //   return (input.length < 3) ? "SubmitApplication.textValidateString".tr(namedArgs: {"length": "3"}) : null;
                                // },
                                onSaveHandler: (input) => (input) => _scholarshipApplicationData["appplicationModel"].name = input.trim(),
                              ),
                              SizedBox(height: scholarshipPageStyles.shareWidthDp * 5),
                              // Text()
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                //// ---- email ---- ////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["email"] != null && _scholarshipApplicationData["formValidate"]["email"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.emailTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomTextFormField(
                                initialValue: _scholarshipApplicationData["appplicationModel"].email,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                borderWidth: 1,
                                hintText: "SubmitApplication.emailTextFieldHintText".tr(),
                                hintTextColor: Colors.grey,
                                hintTextFontSize: scholarshipPageStyles.textFontSize,
                                textFontSize: scholarshipPageStyles.textFontSize,
                                textColor: scholarshipPageColors.textColor,
                                fixedHeightState: true,
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                onChangeHandler: (input) => _scholarshipApplicationData["appplicationModel"].email = input,
                                // validatorHandler: (String input) {
                                //   return (!ScholarshipPageConstants.emailRegex.hasMatch(input)) ? "SubmitApplication.emailValidateString".tr() : null;
                                // },
                                onSaveHandler: (input) => (input) => _scholarshipApplicationData["appplicationModel"].email = input.trim(),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                //// ---- discipline ---- ////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["discipline"] != null &&
                                    _scholarshipApplicationData["formValidate"]["discipline"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.disciplineTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomDropDown(
                                menuItems: ScholarshipPageConstants.disciplineList,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                padding: scholarshipPageStyles.shareWidthDp * 5,
                                hintText: "SubmitApplication.disciplineTextFieldHintText".tr(),
                                hintTextStyle: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.grey),
                                itemTextStyle: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.black),
                                selectedItemTextStyle:
                                    TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                dropdownColor: scholarshipPageColors.dropdownColor,
                                isExpanded: false,
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  size: scholarshipPageStyles.textFontSize * 1.5,
                                  color: scholarshipPageColors.textColor,
                                ),
                                onChangeHandler: (value) {
                                  _scholarshipApplicationData["appplicationModel"].discipline = value;
                                },
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                //// ---- age ---- ////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["age"] != null && _scholarshipApplicationData["formValidate"]["age"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.ageTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomDropDown(
                                menuItems: ageItems,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                padding: scholarshipPageStyles.shareWidthDp * 5,
                                hintText: "SubmitApplication.ageTextFieldHintText".tr(),
                                hintTextStyle: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.grey),
                                itemTextStyle: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.black),
                                selectedItemTextStyle:
                                    TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                dropdownColor: scholarshipPageColors.dropdownColor,
                                isExpanded: false,
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  size: scholarshipPageStyles.textFontSize * 1.5,
                                  color: scholarshipPageColors.textColor,
                                ),
                                onChangeHandler: (value) {
                                  _scholarshipApplicationData["appplicationModel"].age = value;
                                },
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                //// ---- location ---- ////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["location"] != null &&
                                    _scholarshipApplicationData["formValidate"]["location"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.locationTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomTextFormField(
                                initialValue: _scholarshipApplicationData["appplicationModel"].location,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                borderWidth: 1,
                                hintText: "SubmitApplication.locationTextFieldHintText".tr(),
                                hintTextColor: Colors.grey,
                                hintTextFontSize: scholarshipPageStyles.textFontSize,
                                textFontSize: scholarshipPageStyles.textFontSize,
                                textColor: scholarshipPageColors.textColor,
                                fixedHeightState: true,
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                onChangeHandler: (input) => _scholarshipApplicationData["appplicationModel"].location = input.trim(),
                                // validatorHandler: (input) {
                                //   return (input.length < 3) ? "SubmitApplication.textValidateString".tr(namedArgs: {"length": "3"}) : null;
                                // },
                                onSaveHandler: (input) => (input) => _scholarshipApplicationData["appplicationModel"].location = input.trim(),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                //// ---- phoneNumber ---- ////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["phoneNumber"] != null &&
                                    _scholarshipApplicationData["formValidate"]["phoneNumber"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.phoneNumberTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomTextFormField(
                                initialValue: _scholarshipApplicationData["appplicationModel"].phoneNumber,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                borderWidth: 1,
                                hintText: "SubmitApplication.phoneNumberTextFieldHintText".tr(),
                                hintTextColor: Colors.grey,
                                hintTextFontSize: scholarshipPageStyles.textFontSize,
                                textFontSize: scholarshipPageStyles.textFontSize,
                                textColor: scholarshipPageColors.textColor,
                                fixedHeightState: true,
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                onChangeHandler: (input) => _scholarshipApplicationData["appplicationModel"].phoneNumber = input.trim(),
                                // validatorHandler: (input) {
                                //   return (input.length < 6) ? "SubmitApplication.textValidateString".tr(namedArgs: {"length": "6"}) : null;
                                // },
                                onSaveHandler: (input) => (input) => _scholarshipApplicationData["appplicationModel"].phoneNumber = input.trim(),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                //// ---- twitter ---- ////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["twitter"] != null && _scholarshipApplicationData["formValidate"]["twitter"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.twitterTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomTextFormField(
                                initialValue: _scholarshipApplicationData["appplicationModel"].twitter,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                borderWidth: 1,
                                hintText: "SubmitApplication.twitterTextFieldHintText".tr(),
                                hintTextColor: Colors.grey,
                                hintTextFontSize: scholarshipPageStyles.textFontSize,
                                textFontSize: scholarshipPageStyles.textFontSize,
                                textColor: scholarshipPageColors.textColor,
                                fixedHeightState: true,
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                onChangeHandler: (input) => _scholarshipApplicationData["appplicationModel"].twitter = input.trim(),
                                // validatorHandler: (input) {
                                //   return (input.length < 3) ? "SubmitApplication.textValidateString".tr(namedArgs: {"length": "3"}) : null;
                                // },
                                onSaveHandler: (input) => (input) => _scholarshipApplicationData["appplicationModel"].twitter = input.trim(),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                //// ---- instagram ---- ////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["instagram"] != null &&
                                    _scholarshipApplicationData["formValidate"]["instagram"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.instagramTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomTextFormField(
                                initialValue: _scholarshipApplicationData["appplicationModel"].instagram,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                borderWidth: 1,
                                hintText: "SubmitApplication.instagramTextFieldHintText".tr(),
                                hintTextColor: Colors.grey,
                                hintTextFontSize: scholarshipPageStyles.textFontSize,
                                textFontSize: scholarshipPageStyles.textFontSize,
                                textColor: scholarshipPageColors.textColor,
                                fixedHeightState: true,
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                onChangeHandler: (input) => _scholarshipApplicationData["appplicationModel"].instagram = input.trim(),
                                // validatorHandler: (input) {
                                //   return (input.length < 3) ? "SubmitApplication.textValidateString".tr(namedArgs: {"length": "3"}) : null;
                                // },
                                onSaveHandler: (input) => (input) => _scholarshipApplicationData["appplicationModel"].instagram = input.trim(),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                //// ---- facebook ---- ////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                Material(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            (_scholarshipApplicationData["formValidate"]["facebook"] != null &&
                                    _scholarshipApplicationData["formValidate"]["facebook"])
                                ? Icon(Icons.check_circle_outline,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.formValidatedColor)
                                : Icon(Icons.panorama_fish_eye,
                                    size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                            Icon(Icons.edit, size: scholarshipPageStyles.textFontSize * 1.5, color: scholarshipPageColors.textColor)
                          ],
                        ),
                      ),
                      SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                      Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  children: <Widget>[
                                    Text("SubmitApplication.facebookTextFieldLabel".tr(),
                                        style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor)),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    Text("*", style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.red)),
                                  ],
                                ),
                              ),
                              CustomTextFormField(
                                initialValue: _scholarshipApplicationData["appplicationModel"].facebook,
                                width: double.maxFinite,
                                height: scholarshipPageStyles.textFontSize * 2.5,
                                borderType: 1,
                                borderColor: scholarshipPageColors.textColor,
                                borderWidth: 1,
                                hintText: "SubmitApplication.facebookTextFieldHintText".tr(),
                                hintTextColor: Colors.grey,
                                hintTextFontSize: scholarshipPageStyles.textFontSize,
                                textFontSize: scholarshipPageStyles.textFontSize,
                                textColor: scholarshipPageColors.textColor,
                                fixedHeightState: true,
                                fillColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                    ? scholarshipPageColors.backgroundColor
                                    : scholarshipPageColors.panelBackgroundColor,
                                onChangeHandler: (input) => _scholarshipApplicationData["appplicationModel"].facebook = input.trim(),
                                // validatorHandler: (input) {
                                //   return (input.length < 3) ? "SubmitApplication.textValidateString".tr(namedArgs: {"length": "3"}) : null;
                                // },
                                onSaveHandler: (input) => (input) => _scholarshipApplicationData["appplicationModel"].facebook = input.trim(),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                ///////////////////////////
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
                Text(
                  "SubmitApplication.applicationDescription".tr(),
                  style: TextStyle(
                    fontSize: scholarshipPageStyles.textFontSize,
                    color: scholarshipPageColors.textColor,
                    height: 1.5,
                    letterSpacing: 1.5,
                  ),
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
                Divider(
                  height: 1,
                  thickness: 1,
                  color: scholarshipPageColors.textColor,
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),

                //// ---- add your video ---- ////
                CustomDottedBorderContainer(
                  width: (scholarshipPageStyles.scholarshipWidth -
                      ((scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                          ? scholarshipPageStyles.scholarshipHorizontalPadding * 2
                          : scholarshipPageStyles.scholarshipHorizontalPadding * 4)),
                  height: scholarshipPageStyles.shareWidthDp * 150,
                  title: "SubmitApplication.addVideoFieldLabel".tr(),
                  titleMargin: scholarshipPageStyles.shareWidthDp * 10,
                  titlePadding: scholarshipPageStyles.shareWidthDp * 10,
                  titleTextColor: scholarshipPageColors.textColor,
                  titleTextFontSize: scholarshipPageStyles.textFontSize,
                  backgroundColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                      ? scholarshipPageColors.backgroundColor
                      : scholarshipPageColors.panelBackgroundColor,
                  borderWidth: 1,
                  dottedBorderColor: scholarshipPageColors.textColor,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: scholarshipPageStyles.shareWidthDp * 20),
                    child: Material(
                      color: Colors.transparent,
                      child: (_scholarshipApplicationData["videoFile"] == null)
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.cloud_upload,
                                  size: scholarshipPageStyles.titleFontSize * 1.5,
                                  color: scholarshipPageColors.textColor,
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                                GestureDetector(
                                  onTap: () {
                                    _startWebFilePicker(_addVideo);
                                  },
                                  child: Text(
                                    "SubmitApplication.addVideoFieldHintText".tr(),
                                    style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                  ),
                                ),
                              ],
                            )
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _scholarshipApplicationData["videoFile"].name,
                                  style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        width: double.maxFinite,
                                        height: scholarshipPageStyles.shareWidthDp * 5,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(scholarshipPageStyles.shareWidthDp * 5)),
                                            color: scholarshipPageColors.formValidatedColor),
                                      ),
                                    ),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 20),
                                    Icon(Icons.check_circle_outline,
                                        color: scholarshipPageColors.formValidatedColor, size: scholarshipPageStyles.shareWidthDp * 15),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    GestureDetector(
                                      onTap: () {
                                        _scholarshipApplicationData["videoFile"] = null;
                                        scholarshipBloc.add(
                                          UpdatingApplicationDataEvent(
                                            scholarshipID: scholarshipModel.id,
                                            scholarshipApplicationData: _scholarshipApplicationData,
                                          ),
                                        );
                                      },
                                      child: Icon(
                                        Icons.delete,
                                        color: scholarshipPageColors.textColor,
                                        size: scholarshipPageStyles.shareWidthDp * 15,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Text(
                                  "${(((_scholarshipApplicationData["videoFile"].size / 1024 / 1024) * 100).toInt()) / 100} MB",
                                  style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                ),
                              ],
                            ),
                    ),
                  ),
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
                //// ---- Add letter 1 ---- ////
                CustomDottedBorderContainer(
                  width: (scholarshipPageStyles.scholarshipWidth -
                      ((scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                          ? scholarshipPageStyles.scholarshipHorizontalPadding * 2
                          : scholarshipPageStyles.scholarshipHorizontalPadding * 4)),
                  height: scholarshipPageStyles.shareWidthDp * 150,
                  title: "SubmitApplication.addLetter1FieldLabel".tr(),
                  titleMargin: scholarshipPageStyles.shareWidthDp * 10,
                  titlePadding: scholarshipPageStyles.shareWidthDp * 10,
                  titleTextColor: scholarshipPageColors.textColor,
                  titleTextFontSize: scholarshipPageStyles.textFontSize,
                  backgroundColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                      ? scholarshipPageColors.backgroundColor
                      : scholarshipPageColors.panelBackgroundColor,
                  borderWidth: 1,
                  dottedBorderColor: scholarshipPageColors.textColor,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: scholarshipPageStyles.shareWidthDp * 20),
                    child: Material(
                      color: Colors.transparent,
                      child: (_scholarshipApplicationData["letter1File"] == null)
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.cloud_upload,
                                  size: scholarshipPageStyles.titleFontSize * 1.5,
                                  color: scholarshipPageColors.textColor,
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                                GestureDetector(
                                  onTap: () {
                                    _startWebFilePicker(_addLetter1);
                                  },
                                  child: Text(
                                    "SubmitApplication.addLetter1FieldHintText".tr(),
                                    style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                  ),
                                ),
                              ],
                            )
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _scholarshipApplicationData["letter1File"].name,
                                  style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        width: double.maxFinite,
                                        height: scholarshipPageStyles.shareWidthDp * 5,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(scholarshipPageStyles.shareWidthDp * 5)),
                                            color: scholarshipPageColors.formValidatedColor),
                                      ),
                                    ),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 20),
                                    Icon(Icons.check_circle_outline,
                                        color: scholarshipPageColors.formValidatedColor, size: scholarshipPageStyles.shareWidthDp * 15),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    GestureDetector(
                                      onTap: () {
                                        _scholarshipApplicationData["letter1File"] = null;
                                        scholarshipBloc.add(
                                          UpdatingApplicationDataEvent(
                                            scholarshipID: scholarshipModel.id,
                                            scholarshipApplicationData: _scholarshipApplicationData,
                                          ),
                                        );
                                      },
                                      child:
                                          Icon(Icons.delete, color: scholarshipPageColors.textColor, size: scholarshipPageStyles.shareWidthDp * 15),
                                    ),
                                  ],
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Text(
                                  "${(((_scholarshipApplicationData["letter1File"].size / 1024 / 1024) * 100).toInt()) / 100} MB",
                                  style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                ),
                              ],
                            ),
                    ),
                  ),
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
                //// ---- Add letter 2 ---- ////
                CustomDottedBorderContainer(
                  width: double.maxFinite,
                  height: scholarshipPageStyles.shareWidthDp * 150,
                  title: "SubmitApplication.addLetter2FieldLabel".tr(),
                  titleMargin: scholarshipPageStyles.shareWidthDp * 10,
                  titlePadding: scholarshipPageStyles.shareWidthDp * 10,
                  titleTextColor: scholarshipPageColors.textColor,
                  titleTextFontSize: scholarshipPageStyles.textFontSize,
                  backgroundColor: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                      ? scholarshipPageColors.backgroundColor
                      : scholarshipPageColors.panelBackgroundColor,
                  borderWidth: 1,
                  dottedBorderColor: scholarshipPageColors.textColor,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: scholarshipPageStyles.shareWidthDp * 20),
                    child: Material(
                      color: Colors.transparent,
                      child: (_scholarshipApplicationData["letter2File"] == null)
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.cloud_upload,
                                  size: scholarshipPageStyles.titleFontSize * 1.5,
                                  color: scholarshipPageColors.textColor,
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                                GestureDetector(
                                  onTap: () {
                                    _startWebFilePicker(_addLetter2);
                                  },
                                  child: Text(
                                    "SubmitApplication.addLetter2FieldHintText".tr(),
                                    style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                  ),
                                ),
                              ],
                            )
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _scholarshipApplicationData["letter2File"].name,
                                  style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        width: double.maxFinite,
                                        height: scholarshipPageStyles.shareWidthDp * 5,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(scholarshipPageStyles.shareWidthDp * 5)),
                                            color: scholarshipPageColors.formValidatedColor),
                                      ),
                                    ),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 20),
                                    Icon(Icons.check_circle_outline,
                                        color: scholarshipPageColors.formValidatedColor, size: scholarshipPageStyles.shareWidthDp * 15),
                                    SizedBox(width: scholarshipPageStyles.shareWidthDp * 10),
                                    GestureDetector(
                                      onTap: () {
                                        _scholarshipApplicationData["letter2File"] = null;
                                        scholarshipBloc.add(
                                          UpdatingApplicationDataEvent(
                                            scholarshipID: scholarshipModel.id,
                                            scholarshipApplicationData: _scholarshipApplicationData,
                                          ),
                                        );
                                      },
                                      child:
                                          Icon(Icons.delete, color: scholarshipPageColors.textColor, size: scholarshipPageStyles.shareWidthDp * 15),
                                    ),
                                  ],
                                ),
                                SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                                Text(
                                  "${(((_scholarshipApplicationData["letter2File"].size / 1024 / 1024) * 100).toInt()) / 100} MB",
                                  style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: scholarshipPageColors.textColor),
                                ),
                              ],
                            ),
                    ),
                  ),
                ),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
                (scholarshipState.submittingStateForApplication != 2)
                    ? Material(
                        color: Colors.transparent,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: (scholarshipPageStyles.runtimeType == ScholarshipPageMobileStyles)
                                  ? MainAxisAlignment.center
                                  : MainAxisAlignment.start,
                              children: <Widget>[
                                CustomRaisedButton(
                                  width: scholarshipPageStyles.shareWidthDp * 120,
                                  height: scholarshipPageStyles.textFontSize * 2.5,
                                  color: scholarshipPageColors.secondaryColor,
                                  borderRadius: 0,
                                  borderColor: scholarshipPageColors.secondaryColor,
                                  child: Text("SubmitApplication.submitButtonText".tr(),
                                      style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.white)),
                                  onPressed: () {
                                    _submit(context);
                                  },
                                ),
                                SizedBox(width: scholarshipPageStyles.shareWidthDp * 20),
                                CustomRaisedButton(
                                  width: scholarshipPageStyles.shareWidthDp * 120,
                                  height: scholarshipPageStyles.textFontSize * 2.5,
                                  color: scholarshipPageColors.cancelButtonColor,
                                  borderRadius: 0,
                                  borderColor: scholarshipPageColors.cancelButtonColor,
                                  child: Text("SubmitApplication.cancelButtonText".tr(),
                                      style: TextStyle(fontSize: scholarshipPageStyles.textFontSize, color: Colors.black)),
                                  onPressed: () {},
                                ),
                              ],
                            ),
                            SizedBox(height: scholarshipPageStyles.shareWidthDp * 5),
                            (_applicationValdate != null && !_applicationValdate)
                                ? Text(
                                    "Please check fields",
                                    style: TextStyle(fontSize: scholarshipPageStyles.textFontSize * 0.8, color: Colors.red),
                                  )
                                : Text(
                                    "Please check fields",
                                    style: TextStyle(
                                        fontSize: scholarshipPageStyles.textFontSize * 0.8, color: scholarshipPageColors.panelBackgroundColor),
                                  ),
                            (scholarshipState.submittingStateForApplication == -1)
                                ? Text(
                                    "Failed. Please try it",
                                    style: TextStyle(fontSize: scholarshipPageStyles.textFontSize * 0.8, color: Colors.red),
                                  )
                                : SizedBox(),
                          ],
                        ),
                      )
                    : SizedBox(),
                SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _startWebFilePicker(
    Function handler,
  ) async {
    html.InputElement fileInput = html.FileUploadInputElement();
    fileInput.multiple = false;
    fileInput.draggable = true;
    fileInput.accept = '*.*';
    fileInput.click();

    fileInput.onChange.listen((e) {
      // read file content as dataURL
      List<html.File> files = fileInput.files;
      if (files.length == 1) {
        html.File file = files[0];
        html.FileReader reader = html.FileReader();

        reader.onError.listen((fileEvent) {});

        reader.onLoadEnd.listen((e) {
          try {
            Uint8List _bytesData;
            _bytesData = Base64Decoder().convert(reader.result.toString().split(",").last);
            handler(file, _bytesData);
          } catch (e) {
            print(e);
          }
        });

        reader.onProgress.listen((event) {
          event.preventDefault();
        });

        reader.readAsDataUrl(file);
      }
    });
  }

  void _addVideo(html.File file, Uint8List bytesData) {
    _scholarshipApplicationData["videoFile"] = file;
    scholarshipBloc.add(
      UpdatingApplicationDataEvent(
        scholarshipID: scholarshipModel.id,
        scholarshipApplicationData: _scholarshipApplicationData,
      ),
    );
  }

  void _addLetter1(html.File file, Uint8List bytesData) {
    _scholarshipApplicationData["letter1File"] = file;
    scholarshipBloc.add(
      UpdatingApplicationDataEvent(
        scholarshipID: scholarshipModel.id,
        scholarshipApplicationData: _scholarshipApplicationData,
      ),
    );
  }

  void _addLetter2(html.File file, Uint8List bytesData) {
    _scholarshipApplicationData["letter2File"] = file;
    scholarshipBloc.add(
      UpdatingApplicationDataEvent(
        scholarshipID: scholarshipModel.id,
        scholarshipApplicationData: _scholarshipApplicationData,
      ),
    );
  }

  void _submit(BuildContext context) async {
    _scholarshipApplicationData["formValidate"]["name"] = !(_scholarshipApplicationData["appplicationModel"].name.length < 3);
    _scholarshipApplicationData["formValidate"]["email"] =
        ScholarshipPageConstants.emailRegex.hasMatch(_scholarshipApplicationData["appplicationModel"].email);
    _scholarshipApplicationData["formValidate"]["discipline"] = _scholarshipApplicationData["appplicationModel"].discipline != "";
    _scholarshipApplicationData["formValidate"]["age"] = _scholarshipApplicationData["appplicationModel"].age != 0;
    _scholarshipApplicationData["formValidate"]["location"] = !(_scholarshipApplicationData["appplicationModel"].location.length < 3);
    _scholarshipApplicationData["formValidate"]["phoneNumber"] = !(_scholarshipApplicationData["appplicationModel"].phoneNumber.length < 6);
    _scholarshipApplicationData["formValidate"]["twitter"] = !(_scholarshipApplicationData["appplicationModel"].twitter.length < 3);
    _scholarshipApplicationData["formValidate"]["instagram"] = !(_scholarshipApplicationData["appplicationModel"].instagram.length < 3);
    _scholarshipApplicationData["formValidate"]["facebook"] = !(_scholarshipApplicationData["appplicationModel"].facebook.length < 3);
    _applicationValdate = _formValidator();

    if (_applicationValdate) {
      scholarshipBloc.add(
        SubmitApplicationDataEvent(scholarshipModelID: scholarshipModel.id, scholarshipApplicationData: _scholarshipApplicationData),
      );
    } else {
      scholarshipBloc.add(
        UpdatingApplicationDataEvent(scholarshipID: scholarshipModel.id, scholarshipApplicationData: _scholarshipApplicationData),
      );
    }
  }

  dynamic _formValidator() {
    if ((_scholarshipApplicationData["formValidate"]["name"] == null) &&
        (_scholarshipApplicationData["formValidate"]["email"] == null) &&
        (_scholarshipApplicationData["formValidate"]["discipline"] == null) &&
        (_scholarshipApplicationData["formValidate"]["age"] == null) &&
        (_scholarshipApplicationData["formValidate"]["location"] == null) &&
        (_scholarshipApplicationData["formValidate"]["phoneNumber"] == null) &&
        (_scholarshipApplicationData["formValidate"]["twitter"] == null) &&
        (_scholarshipApplicationData["formValidate"]["instagram"] == null) &&
        (_scholarshipApplicationData["formValidate"]["facebook"] == null)) {
      return null;
    } else if ((_scholarshipApplicationData["formValidate"]["name"] != null && _scholarshipApplicationData["formValidate"]["name"]) &&
        (_scholarshipApplicationData["formValidate"]["email"] != null && _scholarshipApplicationData["formValidate"]["email"]) &&
        (_scholarshipApplicationData["formValidate"]["discipline"] != null && _scholarshipApplicationData["formValidate"]["discipline"]) &&
        (_scholarshipApplicationData["formValidate"]["age"] != null && _scholarshipApplicationData["formValidate"]["age"]) &&
        (_scholarshipApplicationData["formValidate"]["location"] != null && _scholarshipApplicationData["formValidate"]["location"]) &&
        (_scholarshipApplicationData["formValidate"]["phoneNumber"] != null && _scholarshipApplicationData["formValidate"]["phoneNumber"]) &&
        (_scholarshipApplicationData["formValidate"]["twitter"] != null && _scholarshipApplicationData["formValidate"]["twitter"]) &&
        (_scholarshipApplicationData["formValidate"]["instagram"] != null && _scholarshipApplicationData["formValidate"]["instagram"]) &&
        (_scholarshipApplicationData["formValidate"]["facebook"] != null && _scholarshipApplicationData["formValidate"]["facebook"])) {
      return true;
    } else {
      return false;
    }
  }
}
