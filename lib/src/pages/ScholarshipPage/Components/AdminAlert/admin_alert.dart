import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'index.dart';

class AdminAlert extends StatelessWidget {
  AdminAlert({
    @required this.type,
    this.width,
  });
  int type;
  double width;
  AdminAlertStyles _adminAlertStyles;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth >= 900) {
          _adminAlertStyles = AdminAlertDesktopStyles(context);
        } else if (constraints.maxWidth >= 600 && constraints.maxWidth < 900) {
          ////  tablet
          _adminAlertStyles = AdminAlertTabletStyles(context);
        } else if (constraints.maxWidth < 600) {
          _adminAlertStyles = AdminAlertMobileStyles(context);
        }
        return Consumer<AdminAlertProvider>(builder: (context, adminAlertProvider, _) {
          return (adminAlertProvider.isShown)
              ? Container(
                  width: (width == null) ? _adminAlertStyles.deviceWidth : width,
                  padding: EdgeInsets.only(
                    left: _adminAlertStyles.primaryHorizontalPadding,
                    right: (_adminAlertStyles.runtimeType == AdminAlertMobileStyles)
                        ? _adminAlertStyles.primaryHorizontalPadding
                        : _adminAlertStyles.primaryHorizontalPadding / 4,
                    top: _adminAlertStyles.primaryVerticalPadding,
                    bottom: _adminAlertStyles.primaryVerticalPadding,
                  ),
                  color: (type == 1) ? AdminAlertColors.backgroundColor1 : AdminAlertColors.backgroundColor2,
                  child: Material(
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            "AdminAlert.alertString1".tr(),
                            style: TextStyle(fontSize: _adminAlertStyles.textFontSize, color: Colors.white),
                          ),
                        ),
                        SizedBox(width: 15),
                        GestureDetector(
                          child: Icon(Icons.close, size: _adminAlertStyles.iconSize, color: Colors.white),
                          onTap: () {
                            adminAlertProvider.setIsShown(false);
                          },
                        )
                      ],
                    ),
                  ),
                )
              : SizedBox();
        });
      }),
    );
  }
}
