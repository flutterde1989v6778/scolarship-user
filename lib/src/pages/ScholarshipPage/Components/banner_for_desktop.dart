import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/assets/images.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';

class BannerForDesktop extends StatelessWidget {
  BannerForDesktop({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
  });
  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            width: scholarshipPageStyles.bannerImageWidth,
            height: scholarshipPageStyles.bannerImageHeight,
            child: Image.asset(
              bannerLandscapeImage,
              width: scholarshipPageStyles.bannerImageWidth,
              height: scholarshipPageStyles.bannerImageHeight,
              fit: BoxFit.cover,
              filterQuality: FilterQuality.low,
            ),
          ),
          Positioned(
            child: Container(
              width: scholarshipPageStyles.bannerImageWidth,
              height: scholarshipPageStyles.bannerImageHeight,
              decoration: BoxDecoration(gradient: scholarshipPageColors.bannerDesktopGradient),
            ),
          ),
          Positioned(
            left: scholarshipPageStyles.bannerTextLeftPadding,
            top: scholarshipPageStyles.bannerTextTopPadding,
            child: Container(
              width: scholarshipPageStyles.bannerTextWidth,
              child: Material(
                color: Colors.transparent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Banner.bannerTitle1".tr(),
                      style: TextStyle(
                          fontSize: scholarshipPageStyles.bannerTitleFontSize, color: scholarshipPageColors.textColor, fontWeight: FontWeight.w200),
                    ),
                    SizedBox(height: scholarshipPageStyles.shareWidthDp * 10),
                    Text(
                      "Banner.bannerTitle2".tr(),
                      style: TextStyle(
                          fontSize: scholarshipPageStyles.bannerTitleFontSize, color: scholarshipPageColors.textColor, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: scholarshipPageStyles.shareWidthDp * 20),
                    Text(
                      "Banner.bannerDescription".tr(),
                      style: TextStyle(
                          fontSize: scholarshipPageStyles.bannerDescriptionFontSize,
                          color: scholarshipPageColors.textColor,
                          height: scholarshipPageStyles.bannerDescriptionLineSpacing,
                          letterSpacing: scholarshipPageStyles.bannerDescriptionLetterSpacing),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
