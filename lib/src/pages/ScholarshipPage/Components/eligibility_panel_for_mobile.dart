import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart' as EL;

import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/widgets/index.dart';
import 'package:scholarship/src/models/scholarship_model.dart';
import 'package:scholarship/src/pages/ScholarshipPage/Styles/index.dart';

class EligibilityPanelForMobile extends StatelessWidget {
  EligibilityPanelForMobile({
    @required this.scholarshipPageStyles,
    @required this.scholarshipPageColors,
    @required this.scholarshipBloc,
    @required this.scholarshipState,
    @required this.scholarshipModel,
  });

  ScholarshipBloc scholarshipBloc;
  ScholarshipState scholarshipState;
  ScholarshipModel scholarshipModel;

  ScholarshipPageStyles scholarshipPageStyles;
  ScholarshipPageColors scholarshipPageColors;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        width: scholarshipPageStyles.scholarshipWidth,
        padding: EdgeInsets.symmetric(
          vertical: scholarshipPageStyles.scholarshipVerticalPadding,
        ),
        child: Material(
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Eligibility.titleForMobile".tr(),
                style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.textColor, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
              Text(
                ScholarshipPageString.eligibilitySubTitle1,
                style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.textColor, height: 1.5),
              ),
              Text(
                ScholarshipPageString.eligibilityDescription1,
                style: TextStyle(
                  fontSize: scholarshipPageStyles.textFontSize,
                  color: scholarshipPageColors.textColor,
                  height: 1.5,
                  letterSpacing: 1.5,
                ),
              ),
              SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
              Text(
                ScholarshipPageString.eligibilitySubTitle2,
                style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.textColor, height: 1.5),
              ),
              Text(
                ScholarshipPageString.eligibilityDescription2,
                style: TextStyle(
                  fontSize: scholarshipPageStyles.textFontSize,
                  color: scholarshipPageColors.textColor,
                  height: 1.5,
                  letterSpacing: 1.5,
                ),
              ),
              SizedBox(height: scholarshipPageStyles.shareWidthDp * 30),
              Text(
                ScholarshipPageString.eligibilitySubTitle3,
                style: TextStyle(fontSize: scholarshipPageStyles.titleFontSize, color: scholarshipPageColors.textColor, height: 1.5),
              ),
              Text(
                ScholarshipPageString.eligibilityDescription3,
                style: TextStyle(
                  fontSize: scholarshipPageStyles.textFontSize,
                  color: scholarshipPageColors.textColor,
                  height: 1.5,
                  letterSpacing: 1.5,
                ),
              ),
              SizedBox(height: scholarshipPageStyles.shareWidthDp * 50),
            ],
          ),
        ),
      ),
    );
  }
}
