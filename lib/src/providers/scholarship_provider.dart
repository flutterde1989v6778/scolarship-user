import 'dart:async';

import 'package:scholarship/src/models/index.dart';

import 'index.dart';

class ScholarshipProvider extends FirebaseProvider {
  final String collectionName = "/Scholarships";
  FirebaseProvider firebaseProvider = FirebaseProvider();

  Future<ScholarshipModel> addScholarship({ScholarshipModel scholarshipModel}) async {
    try {
      var result = await firebaseProvider.addDocument(collectionName: collectionName, data: scholarshipModel.toJson());
      if (result != "-1")
        return ScholarshipModel.fromJson(result);
      else
        return null;
    } catch (e) {
      print("addScholarship error");
      print(e);
      return null;
    }
  }

  Future<bool> updateScholarship({ScholarshipModel scholarshipModel}) async {
    return await firebaseProvider.updateDocument(collectionName: collectionName, id: scholarshipModel.id, data: scholarshipModel.toJson());
  }

  Future<bool> deleteScholarship({ScholarshipModel scholarshipModel}) async {
    return await firebaseProvider.deleteDocument(collectionName: collectionName, id: scholarshipModel.id);
  }

  Stream<List<ScholarshipModel>> getScholarshipListStream({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) {
    try {
      Stream<List<Map<String, dynamic>>> stream =
          firebaseProvider.getDocumentsStream(collectionName: collectionName, wheres: wheres, orderby: orderby, limit: limit);
      return stream.map((dataList) {
        return dataList.map((data) {
          return ScholarshipModel.fromJson(data);
        }).toList();
      });
    } catch (e) {
      return null;
    }
  }

  Future<List<ScholarshipModel>> getScholarshipList({
    List<Map<String, dynamic>> wheres,
    List<Map<String, dynamic>> orderby,
    int limit,
  }) async {
    List<ScholarshipModel> scholarshipModelList = [];
    var result = await firebaseProvider.getDocumentData(collectionName: collectionName, wheres: wheres, orderby: orderby, limit: limit);
    if (result != "-1") {
      result.forEach((data) {
        scholarshipModelList.add(ScholarshipModel.fromJson(data));
      });
      return scholarshipModelList;
    } else {
      return null;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }
}
