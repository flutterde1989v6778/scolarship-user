import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scholarship/src/Config/resposible_settings.dart';

class ScholarshipPageStyles {
  double devicePixelRatio;
  double deviceWidth;
  double deviceHeight;
  double fontUnit;
  double statusbarHeight;
  double bottombarHeight;
  double appbarHeight;
  double safeAreaHeight;
  double shareWidth;
  double shareHeight;

  double shareWidthDp;
  double shareHeightDp;

  double primaryHorizontalPadding;
  double primaryVerticalPadding;

  /// main Style
  double titleFontSize;
  double textFontSize;
  double bigTitleFontSize;
  double lineHeight;

  /// banner
  double bannerImageWidth;
  double bannerImageHeight;
  double bannerTextWidth;
  double bannerTitleFontSize;
  double bannerDescriptionFontSize;
  double bannerTextTopPadding;
  double bannerTextBottomPadding;
  double bannerTextLeftPadding;
  double bannerDescriptionLineSpacing;
  double bannerDescriptionLetterSpacing;

  /// choose panel
  double choosePanelHorizontalMargin;
  double choosePanelHorizontalPadding;
  double choosePanelVerticalPadding;
  double choosePanelLineSpacing;
  double choosePanelLetterSpacing;

  /// carousel panel for mobile
  double carouselMargin;
  double carouselIconSize;
  double carouselWidth;
  double carouselHeight;

  /// scholarship opportunity
  double opportunityHorizontalPadding;
  double opportunityVerticalPadding;

  /// scholarshipList panel
  double scholarshipHorizontalMargin;
  double scholarshipVerticalMargin;
  double scholarshipHorizontalPadding;
  double scholarshipVerticalPadding;
  double scholarshipWidth;
  // double scholarshipHeight;
  double scholarshipTitleFontSize;
  double scholarshipImageWidth;
  double scholarshipImageHeight;
  double scholarshipImageSpacing;
  double scholarshipDetailLinkTextFontSize;
  double shcolarshipVideoWidth;

  /// Eligibility
  double eligibilitySubtitleFontSize;
  double eligibilityCardWidth;
  double eligibilityCardHeight;
  double eligibilityCardHorizontalPadding;
  double eligibilityCardVerticalPadding;

  ScholarshipPageStyles(BuildContext context) {}
}

class ScholarshipPageDesktopStyles extends ScholarshipPageStyles {
  ScholarshipPageDesktopStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(20);
    primaryVerticalPadding = ScreenUtil().setWidth(24);

    /// mainStyle
    titleFontSize = ScreenUtil().setSp(20, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);
    bigTitleFontSize = ScreenUtil().setSp(38, allowFontScalingSelf: false);
    lineHeight = ScreenUtil().setWidth(3);

    /// banner
    bannerImageWidth = deviceWidth;
    bannerImageHeight = bannerImageWidth * 540 / 940;
    bannerTextWidth = ScreenUtil().setWidth(511);
    bannerTextTopPadding = bannerImageHeight * 0.25;
    bannerTextLeftPadding = ScreenUtil().setWidth(33);
    bannerTitleFontSize = ScreenUtil().setSp(48, allowFontScalingSelf: false);
    bannerDescriptionFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    bannerDescriptionLineSpacing = 1.5;
    bannerDescriptionLetterSpacing = 1.5;

    /// choose panel
    choosePanelHorizontalMargin = ScreenUtil().setWidth(46);
    choosePanelHorizontalPadding = ScreenUtil().setWidth(47);
    choosePanelVerticalPadding = ScreenUtil().setWidth(25);
    choosePanelLineSpacing = 1.5;
    choosePanelLetterSpacing = 1.5;

    /// scholarship opportunity
    opportunityHorizontalPadding = ScreenUtil().setWidth(37);
    opportunityVerticalPadding = ScreenUtil().setWidth(37);

    /// scholarshipList panel
    scholarshipHorizontalMargin = ScreenUtil().setWidth(125);
    scholarshipVerticalMargin = ScreenUtil().setWidth(15);
    scholarshipHorizontalPadding = ScreenUtil().setWidth(40);
    scholarshipVerticalPadding = ScreenUtil().setWidth(30);
    scholarshipWidth = deviceWidth - scholarshipHorizontalMargin * 2;
    // scholarshipHeight = ScreenUtil().setWidth(215);
    scholarshipImageWidth = ScreenUtil().setWidth(204);
    scholarshipImageHeight = ScreenUtil().setWidth(155);
    scholarshipImageSpacing = ScreenUtil().setWidth(55);
    scholarshipDetailLinkTextFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    shcolarshipVideoWidth = ScreenUtil().setWidth(770);

    /// Eligibility
    eligibilitySubtitleFontSize = ScreenUtil().setSp(16, allowFontScalingSelf: false);
    eligibilityCardWidth = ScreenUtil().setWidth(278);
    eligibilityCardHeight = ScreenUtil().setWidth(172);
    eligibilityCardHorizontalPadding = ScreenUtil().setWidth(18);
    eligibilityCardVerticalPadding = ScreenUtil().setWidth(15);
  }
}

class ScholarshipPageTabletStyles extends ScholarshipPageStyles {
  ScholarshipPageTabletStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.desktopDesignWidth,
      height: ResponsibleDesignSettings.desktopDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(20);
    primaryVerticalPadding = ScreenUtil().setWidth(24);

    /// mainStyle
    titleFontSize = ScreenUtil().setSp(20, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);
    bigTitleFontSize = ScreenUtil().setSp(38, allowFontScalingSelf: false);
    lineHeight = ScreenUtil().setWidth(3);

    /// banner
    bannerImageWidth = deviceWidth;
    bannerImageHeight = bannerImageWidth * 540 / 940;
    bannerTextWidth = ScreenUtil().setWidth(511);
    bannerTextTopPadding = bannerImageHeight * 0.25;
    bannerTextLeftPadding = ScreenUtil().setWidth(33);
    bannerTitleFontSize = ScreenUtil().setSp(48, allowFontScalingSelf: false);
    bannerDescriptionFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    bannerDescriptionLineSpacing = 1.5;
    bannerDescriptionLetterSpacing = 1.5;

    /// choose panel
    choosePanelHorizontalMargin = ScreenUtil().setWidth(46);
    choosePanelHorizontalPadding = ScreenUtil().setWidth(47);
    choosePanelVerticalPadding = ScreenUtil().setWidth(25);
    choosePanelLineSpacing = 1.5;
    choosePanelLetterSpacing = 1.5;

    /// scholarship opportunity
    opportunityHorizontalPadding = ScreenUtil().setWidth(37);
    opportunityVerticalPadding = ScreenUtil().setWidth(37);

    /// scholarshipList panel
    scholarshipHorizontalMargin = ScreenUtil().setWidth(125);
    scholarshipVerticalMargin = ScreenUtil().setWidth(15);
    scholarshipHorizontalPadding = ScreenUtil().setWidth(40);
    scholarshipVerticalPadding = ScreenUtil().setWidth(30);
    scholarshipWidth = deviceWidth - scholarshipHorizontalMargin * 2;
    // scholarshipHeight = ScreenUtil().setWidth(215);
    scholarshipImageWidth = ScreenUtil().setWidth(204);
    scholarshipImageHeight = ScreenUtil().setWidth(155);
    scholarshipImageSpacing = ScreenUtil().setWidth(55);
    scholarshipDetailLinkTextFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    shcolarshipVideoWidth = ScreenUtil().setWidth(770);

    /// Eligibility
    eligibilitySubtitleFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    eligibilityCardWidth = ScreenUtil().setWidth(278);
    eligibilityCardHeight = ScreenUtil().setWidth(172);
    eligibilityCardHorizontalPadding = ScreenUtil().setWidth(18);
    eligibilityCardVerticalPadding = ScreenUtil().setWidth(15);
  }
}

class ScholarshipPageMobileStyles extends ScholarshipPageStyles {
  ScholarshipPageMobileStyles(BuildContext context) : super(context) {
    ScreenUtil.init(
      context,
      width: ResponsibleDesignSettings.mobileDesignWidth,
      height: ResponsibleDesignSettings.mobileDesignHeight,
      allowFontScaling: false,
    );

    devicePixelRatio = ScreenUtil.pixelRatio;
    deviceWidth = ScreenUtil.screenWidthDp;
    deviceHeight = ScreenUtil.screenHeightDp;
    statusbarHeight = ScreenUtil.statusBarHeight;
    appbarHeight = AppBar().preferredSize.height;
    bottombarHeight = ScreenUtil.bottomBarHeight;
    safeAreaHeight = deviceHeight - statusbarHeight - appbarHeight - bottombarHeight;
    shareWidth = deviceWidth / 100;
    shareHeight = deviceHeight / 100;
    shareWidthDp = ScreenUtil().setWidth(1);
    shareHeightDp = ScreenUtil().setHeight(1);

    primaryHorizontalPadding = ScreenUtil().setWidth(22);
    primaryVerticalPadding = ScreenUtil().setWidth(24);

    /// mainStyle
    titleFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);
    textFontSize = ScreenUtil().setSp(12, allowFontScalingSelf: false);
    bigTitleFontSize = ScreenUtil().setSp(24, allowFontScalingSelf: false);
    lineHeight = ScreenUtil().setWidth(3);

    /// banner
    bannerImageWidth = deviceWidth;
    bannerImageHeight = safeAreaHeight;
    bannerTextLeftPadding = ScreenUtil().setWidth(20);
    bannerTextBottomPadding = ScreenUtil().setWidth(25);
    bannerTextWidth = deviceWidth - bannerTextLeftPadding * 2;
    bannerTitleFontSize = ScreenUtil().setSp(20, allowFontScalingSelf: false);
    bannerDescriptionFontSize = ScreenUtil().setSp(14, allowFontScalingSelf: false);

    /// choose panel for mobile
    choosePanelHorizontalPadding = ScreenUtil().setWidth(20);
    choosePanelVerticalPadding = ScreenUtil().setWidth(20);

    /// carousel for mobile
    carouselWidth = ScreenUtil().setWidth(292);
    carouselHeight = ScreenUtil().setWidth(169);
    carouselMargin = ScreenUtil().setWidth(18);
    carouselIconSize = carouselHeight - choosePanelVerticalPadding * 2;

    /// scholarship opportunity
    opportunityHorizontalPadding = ScreenUtil().setWidth(22);
    opportunityVerticalPadding = ScreenUtil().setWidth(22);

    /// scholarshipList panel
    scholarshipTitleFontSize = ScreenUtil().setSp(18, allowFontScalingSelf: false);
    scholarshipHorizontalMargin = ScreenUtil().setWidth(23);
    scholarshipVerticalMargin = ScreenUtil().setWidth(15);
    scholarshipHorizontalPadding = ScreenUtil().setWidth(5);
    scholarshipVerticalPadding = ScreenUtil().setWidth(5);
    scholarshipWidth = deviceWidth - scholarshipHorizontalMargin * 2;
    // scholarshipHeight = 150;
    scholarshipImageWidth = scholarshipWidth;
    scholarshipImageHeight = scholarshipImageWidth * 0.6;
    shcolarshipVideoWidth = scholarshipWidth;
  }
}
