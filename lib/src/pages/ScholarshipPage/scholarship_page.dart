import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scholarship/src/blocs/Blocs/index.dart';
import 'package:scholarship/src/blocs/ScholarshipBloc/index.dart';
import 'package:scholarship/src/widgets/SettingMenu/index.dart';

import './index.dart';

class ScholarshipPage extends StatelessWidget {
  ScholarshipPageColors _scholarshipPageColors;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: BlocBuilder<ThemeBloc, ThemeState>(
        builder: (context, state) {
          if (state.themeMode == ThemeModeConstants.dark) {
            _scholarshipPageColors = ScholarshipPageDarkModeColors();
          } else {
            _scholarshipPageColors = ScholarshipPageLightModeColors();
          }
          return Scaffold(
            appBar: AppBar(
              title: Text('Scholarship'),
              backgroundColor: Color(0xFF232323),
              actions: <Widget>[
                SettingMenu(),
              ],
            ),
            backgroundColor: _scholarshipPageColors.backgroundColor,
            body: MultiBlocProvider(
              providers: [
                BlocProvider<ScholarshipBloc>(create: (context) => ScholarshipBloc()),
              ],
              child: ScholarshipView(scholarshipPageColors: _scholarshipPageColors),
            ),
          );
        },
      ),
    );
  }
}
